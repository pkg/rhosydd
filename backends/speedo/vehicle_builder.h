/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef SPEEDO_VEHICLE_BUILDER_H
#define SPEEDO_VEHICLE_BUILDER_H

#include <glib.h>

GPtrArray /*RsdAttributeInfo*/ *build_vehicle (void);
void                           destroy_vehicle (void);

#endif /* !SPEEDO_VEHICLE_BUILDER_H */
