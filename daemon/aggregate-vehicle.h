/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef VDD_AGGREGATE_VEHICLE_H
#define VDD_AGGREGATE_VEHICLE_H

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include "librhosydd/vehicle.h"

G_BEGIN_DECLS

#define VDD_TYPE_AGGREGATE_VEHICLE vdd_aggregate_vehicle_get_type ()
G_DECLARE_FINAL_TYPE (VddAggregateVehicle, vdd_aggregate_vehicle, VDD,
                      AGGREGATE_VEHICLE, GObject)

void         vdd_aggregate_vehicle_update_vehicles (VddAggregateVehicle *self,
                                                    RsdVehicle          *added,
                                                    RsdVehicle          *removed);
GPtrArray   *vdd_aggregate_vehicle_get_vehicles    (VddAggregateVehicle *self);

GPtrArray   *vdd_aggregate_vehicle_get_zones       (VddAggregateVehicle  *self,
                                                    const gchar          *parent_zone_path,
                                                    const gchar * const  *tags,
                                                    GError              **error);

VddAggregateVehicle *vdd_aggregate_vehicle_new     (const gchar          *id);

G_END_DECLS

#endif /* !VDD_AGGREGATE_VEHICLE_H */
