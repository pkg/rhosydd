/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef VDD_VEHICLE_MULTIPLEXER_H
#define VDD_VEHICLE_MULTIPLEXER_H

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include "libcroesor/vehicle-manager.h"
#include "librhosydd/vehicle.h"

G_BEGIN_DECLS

void vdd_vehicle_multiplexer_update_vehicles (GPtrArray             *added,
                                              GPtrArray             *removed,
                                              CsrVehicleManager     *output_manager);

G_END_DECLS

#endif /* !VDD_VEHICLE_MULTIPLEXER_H */
