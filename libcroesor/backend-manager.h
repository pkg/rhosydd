/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CSR_BACKEND_MANAGER_H
#define CSR_BACKEND_MANAGER_H

#include <glib.h>
#include <glib-object.h>

#include <libcroesor/peer-manager.h>

G_BEGIN_DECLS

/**
 * CSR_TYPE_BACKEND_MANAGER:
 *
 * Retrieve the #CsrBackendMAnager type.
 *
 * Since: 0.2.0
 */
#define CSR_TYPE_BACKEND_MANAGER csr_backend_manager_get_type ()
G_DECLARE_FINAL_TYPE (CsrBackendManager, csr_backend_manager, CSR,
                      BACKEND_MANAGER, GObject)

CsrBackendManager *csr_backend_manager_new (void);

G_END_DECLS

#endif /* !CSR_BACKEND_MANAGER_H */
