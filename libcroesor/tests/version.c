/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2017 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>

#include "libcroesor/version.h"

/* This is mostly here for its side-effect of asserting that the
 * major, minor, micro versions are numeric. */
static void
test_version (void)
{
  g_assert_true (CSR_CHECK_VERSION (0, 1706, 0));
}

int
main (int argc, char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/version", test_version);

  return g_test_run ();
}
