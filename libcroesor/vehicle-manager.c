/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>

#include "libcroesor/vehicle-manager.h"
#include "libinternal/logging.h"
#include "librhosydd/vehicle.h"

/**
 * SECTION:vehicle-manager
 * @short_description: A manager object which stores a set of #RsdVehicles
 *
 * See also #CsrVehicleManager
 *
 * #vehicle-manager is available since 0.1.0
 */

static void csr_vehicle_manager_dispose      (GObject      *object);

static void csr_vehicle_manager_get_property (GObject      *object,
                                              guint         property_id,
                                              GValue       *value,
                                              GParamSpec   *pspec);
static void csr_vehicle_manager_set_property (GObject      *object,
                                              guint         property_id,
                                              const GValue *value,
                                              GParamSpec   *pspec)
                                              G_GNUC_NORETURN;

/**
 * CsrVehicleManager:
 *
 * A manager object which stores a set of #RsdVehicles and allows managing them
 * using bulk add and remove operations.
 *
 * Since: 0.1.0
 */
struct _CsrVehicleManager
{
  GObject parent;

  /* Mapping from vehicle ID to vehicle. */
  GHashTable/*<owned utf8, owned RsdVehicle>*/ *vehicles;  /* owned */
};

typedef enum
{
  PROP_VEHICLES = 1,
} CsrVehicleManagerProperty;

G_DEFINE_TYPE (CsrVehicleManager, csr_vehicle_manager, G_TYPE_OBJECT)

static void
csr_vehicle_manager_class_init (CsrVehicleManagerClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GParamSpec *props[PROP_VEHICLES + 1] = { NULL, };

  object_class->dispose = csr_vehicle_manager_dispose;
  object_class->get_property = csr_vehicle_manager_get_property;
  object_class->set_property = csr_vehicle_manager_set_property;

  /**
   * CsrVehicleManager:vehicles: (type GHashTable(utf8,RsdVehicle)) (transfer none)
   *
   * Set of vehicles known to the manager, which might be empty. It is a mapping
   * from vehicle ID to #RsdVehicle instance. Use
   * csr_vehicle_manager_update_vehicles() to modify the set.
   *
   * Since: 0.1.0
   */
  props[PROP_VEHICLES] =
      g_param_spec_boxed ("vehicles", "Vehicles",
                          "Set of vehicles known to the manager.",
                          G_TYPE_HASH_TABLE,
                          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (props), props);

  /**
   * CsrVehicleManager::vehicles-changed:
   * @self: a #CsrVehicleManager
   * @added: (element-type RsdVehicle): potentially empty or %NULL array of
   *     added vehicles
   * @removed: (element-type RsdVehicle): potentially empty or %NULL array of
   *     removed vehicles
   *
   * Emitted when the set of vehicles known the manager changes. It is emitted
   * at the same time as #GObject::notify for the #CsrVehicleManager:vehicles
   * property, but contains the delta of which vehicles have been added and
   * removed.
   *
   * There will be at least one vehicle in one of the arrays.
   *
   * Since: 0.2.0
   */
  g_signal_new ("vehicles-changed", G_TYPE_FROM_CLASS (klass),
                G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                G_TYPE_NONE, 2, G_TYPE_PTR_ARRAY, G_TYPE_PTR_ARRAY);
}

static void
csr_vehicle_manager_init (CsrVehicleManager *self)
{
  self->vehicles = g_hash_table_new_full (g_str_hash, g_str_equal,
                                          g_free, g_object_unref);
}

static void
csr_vehicle_manager_dispose (GObject *object)
{
  CsrVehicleManager *self = CSR_VEHICLE_MANAGER (object);

  g_clear_pointer (&self->vehicles, g_hash_table_unref);

  /* Chain up to the parent class */
  G_OBJECT_CLASS (csr_vehicle_manager_parent_class)->dispose (object);
}

static void
csr_vehicle_manager_get_property (GObject    *object,
                                  guint       property_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  CsrVehicleManager *self = CSR_VEHICLE_MANAGER (object);

  switch ((CsrVehicleManagerProperty) property_id)
    {
    case PROP_VEHICLES:
      g_value_set_boxed (value, self->vehicles);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
csr_vehicle_manager_set_property (GObject      *object,
                                  guint         property_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  switch ((CsrVehicleManagerProperty) property_id)
    {
    case PROP_VEHICLES:
      /* Read only. */
      g_assert_not_reached ();
      break;
    default:
      g_assert_not_reached ();
    }
}

/**
 * csr_vehicle_manager_new:
 *
 * Create a new #CsrVehicleManager instance, with no vehicles to begin with.
 *
 * Returns: (transfer full): a new #CsrVehicleManager
 * Since: 0.1.0
 */
CsrVehicleManager *
csr_vehicle_manager_new (void)
{
  return g_object_new (CSR_TYPE_VEHICLE_MANAGER, NULL);
}

/* Needed to avoid strict aliasing problems with the
 * g_hash_table_lookup_extended() call below. */
typedef void AutoObjectPointer;

G_DEFINE_AUTOPTR_CLEANUP_FUNC (AutoObjectPointer, g_object_unref)

/**
 * csr_vehicle_manager_update_vehicles:
 * @self: a #CsrVehicleManager
 * @added: (nullable) (transfer none) (element-type RsdVehicle): set of
 *    #RsdVehicle instances to add to the manager
 * @removed: (nullable) (transfer none) (element-type RsdVehicle): set of
 *    #RsdVehicle instances to remove from the manager
 *
 * Update the set of vehicles in the manager, adding all vehicles in @added, and
 * removing all those in @removed. If a vehicle with the same ID appears in both
 * arrays, the old #RsdVehicle instance for it will be removed from the
 * #CsrVehicleManager and replaced with the new one from @added.
 *
 * Vehicles in @added which are already in the manager are ignored; vehicles in
 * @removed which are not in the manager are ignored.
 *
 * Since: 0.1.0
 */
void
csr_vehicle_manager_update_vehicles (CsrVehicleManager *self,
                                     GPtrArray         *added,
                                     GPtrArray         *removed)
{
  gsize i;
  g_autoptr (GPtrArray/*<unowned RsdVehicle>*/) actually_added = NULL;
  g_autoptr (GPtrArray/*<unowned RsdVehicle>*/) actually_removed = NULL;

  g_return_if_fail (CSR_IS_VEHICLE_MANAGER (self));

  actually_added = g_ptr_array_new ();
  actually_removed = g_ptr_array_new ();

  for (i = 0; removed != NULL && i < removed->len; i++)
    {
      RsdVehicle *vehicle = removed->pdata[i];
      g_autofree gpointer stolen_id = NULL;  /* actually gchar* */
      g_autoptr (AutoObjectPointer) stolen_vehicle = NULL;  /* RsdVehicle* */

      g_return_if_fail (RSD_IS_VEHICLE (vehicle));

      DEBUG ("Removing vehicle ‘%s’.", rsd_vehicle_get_id (vehicle));

      /* Steal the key so we can ensure it doesn’t get freed before adding it
       * to the @actually_removed array. */
      if (g_hash_table_lookup_extended (self->vehicles,
                                        rsd_vehicle_get_id (vehicle),
                                        &stolen_id,
                                        &stolen_vehicle))
        {
          g_hash_table_steal (self->vehicles, stolen_id);
          g_ptr_array_add (actually_removed, g_steal_pointer (&stolen_vehicle));
        }
      else
        {
          DEBUG ("Vehicle ‘%s’ did not exist in CsrVehicleManager %p.",
                 rsd_vehicle_get_id (vehicle), self);
        }
    }

  for (i = 0; added != NULL && i < added->len; i++)
    {
      RsdVehicle *vehicle = added->pdata[i];

      g_return_if_fail (RSD_IS_VEHICLE (vehicle));

      DEBUG ("Adding vehicle ‘%s’.", rsd_vehicle_get_id (vehicle));

      if (g_hash_table_insert (self->vehicles,
                               g_strdup (rsd_vehicle_get_id (vehicle)),
                               g_object_ref (vehicle)))
        g_ptr_array_add (actually_added, vehicle);
      else
        DEBUG ("Vehicle ‘%s’ already existed in CsrVehicleManager %p.",
               rsd_vehicle_get_id (vehicle), self);
    }

  if (actually_added->len > 0 || actually_removed->len > 0)
    {
      g_object_notify (G_OBJECT (self), "vehicles");
      g_signal_emit_by_name (G_OBJECT (self), "vehicles-changed",
                             actually_added, actually_removed);
    }
}

/**
 * csr_vehicle_manager_get_vehicle:
 * @self: a #CsrVehicleManager
 * @vehicle_id: ID of the vehicle to look up
 *
 * Look up the given @vehicle_id in the manager and return it if found;
 * otherwise return %NULL.
 *
 * Returns: (transfer none) (nullable): the found vehicle, or %NULL
 * Since: 0.1.0
 */
RsdVehicle *
csr_vehicle_manager_get_vehicle (CsrVehicleManager *self,
                                 const gchar       *vehicle_id)
{
  g_return_val_if_fail (CSR_IS_VEHICLE_MANAGER (self), NULL);
  g_return_val_if_fail (rsd_vehicle_id_is_valid (vehicle_id), NULL);

  return g_hash_table_lookup (self->vehicles, vehicle_id);
}

/**
 * csr_vehicle_manager_get_vehicles:
 * @self: a #CsrVehicleManager
 *
 * Get the complete set of vehicles known to the manager, as a hash table
 * mapping vehicle ID to #RsdVehicle instance.
 *
 * Returns: (transfer none) (element-type utf8 RsdVehicle): mapping of vehicle
 *    IDs to vehicles
 * Since: 0.1.0
 */
GHashTable *
csr_vehicle_manager_get_vehicles (CsrVehicleManager *self)
{
  g_return_val_if_fail (CSR_IS_VEHICLE_MANAGER (self), NULL);

  return self->vehicles;
}
