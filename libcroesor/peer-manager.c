/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <polkit/polkit.h>

#include "peer-manager.h"

/**
 * SECTION:peer-manager
 * @short_description: An object instance which implements #CsrPeerManagerInterface.
 *
 * See also #CsrPeerManager
 *
 * #peer-manager is available since 0.2.0
 */

G_DEFINE_INTERFACE (CsrPeerManager, csr_peer_manager, G_TYPE_OBJECT)

static void
csr_peer_manager_default_init (CsrPeerManagerInterface *iface)
{
  GParamSpec *pspec;

  /* No default implementations. */

  /**
   * CsrPeerManager:peers: (type GStrv) (transfer none)
   *
   * Set of peers known to the manager, which might be empty. It is an array of
   * the D-Bus unique names of the known peers. Peers are added to it by calling
   * csr_peer_manager_ensure_peer_info_async().
   *
   * Since: 0.2.0
   */
  pspec = g_param_spec_boxed ("peers", "Peers",
                              "Set of peers known to the manager.",
                              G_TYPE_STRV,
                              G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);
  g_object_interface_install_property (iface, pspec);

  /**
   * CsrPeerManager::peer-vanished:
   * @self: a #CsrPeerManager
   * @peer: unique D-Bus name of the peer
   *
   * Emitted when a peer disappears from the bus (either gracefully or
   * ungracefully).
   *
   * Since: 0.2.0
   */
  g_signal_new ("peer-vanished", G_TYPE_FROM_INTERFACE (iface),
                G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                G_TYPE_NONE, 1, G_TYPE_STRING);
}

/**
 * csr_peer_manager_ensure_peer_info_async:
 * @peer_manager: a #CsrPeerManager
 * @sender: unique name of the D-Bus peer
 * @connection: D-Bus connection for @sender
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to invoke on completion of the asynchronous method
 * @user_data: data to pass to @callback
 *
 * Get the #CbyProcessInfo for the peer making a D-Bus method call described by
 * @sender. The returned process infos are cached, and are only cleared when
 * the corresponding D-Bus connection vanishes.
 *
 * See the documentation for #CbyProcessInfo for information about its
 * semantics, including how to avoid time-of-check-to-time-of-use race
 * conditions when using it for authorisation.
 *
 * Since: 0.2.0
 */
void
csr_peer_manager_ensure_peer_info_async (CsrPeerManager      *peer_manager,
                                         const gchar         *sender,
                                         GDBusConnection     *connection,
                                         GCancellable        *cancellable,
                                         GAsyncReadyCallback  callback,
                                         gpointer             user_data)
{
  CsrPeerManagerInterface *iface;

  g_return_if_fail (CSR_IS_PEER_MANAGER (peer_manager));
  g_return_if_fail (sender != NULL && g_dbus_is_unique_name (sender));
  g_return_if_fail (G_IS_DBUS_CONNECTION (connection));
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  iface = CSR_PEER_MANAGER_GET_IFACE (peer_manager);
  g_assert (iface->ensure_peer_info_async != NULL);
  iface->ensure_peer_info_async (peer_manager, sender, connection, cancellable,
                                 callback, user_data);
}

/**
 * csr_peer_manager_ensure_peer_info_finish:
 * @peer_manager: a #CsrPeerManager
 * @result: asynchronous result
 * @error: return location for a #GError
 *
 * See csr_peer_manager_ensure_peer_info_async().
 *
 * Returns: (transfer full): information about the peer process
 * Since: 0.2.0
 */
void *
csr_peer_manager_ensure_peer_info_finish (CsrPeerManager  *peer_manager,
                                          GAsyncResult    *result,
                                          GError         **error)
{
  CsrPeerManagerInterface *iface;
  void *retval = NULL;
  g_autoptr (GError) child_error = NULL;

  g_return_val_if_fail (CSR_IS_PEER_MANAGER (peer_manager), NULL);
  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  iface = CSR_PEER_MANAGER_GET_IFACE (peer_manager);
  g_assert (iface->ensure_peer_info_finish != NULL);
  retval = iface->ensure_peer_info_finish (peer_manager, result, &child_error);

  g_return_val_if_fail ((retval == NULL) != (child_error == NULL), NULL);

  if (child_error != NULL)
    g_propagate_error (error, g_steal_pointer (&child_error));

  return g_steal_pointer (&retval);
}

/**
 * csr_peer_manager_check_authorization_async:
 * @peer_manager: a #CsrPeerManager
 * @sender: D-Bus unique name of the peer which is making a request
 * @connection: #GDBusConnection associated with @sender
 * @action_id: ID of the polkit action to check for authorisation
 * @details: populated #PolkitDetails object to pass to the authorisation check
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to call once the check is complete
 * @user_data: data to pass to @callback
 *
 * Check that the peer process which is making a D-Bus call to this process is
 * sufficiently authorised to do so, by checking that its #CbyProcessInfo
 * process type is not %CBY_PROCESS_TYPE_UNKNOWN; and that it is allowed to do
 * the polkit action identified by @action_id and @details.
 *
 * If the process is authorised, nothing will be returned. If it is not
 * authorised, or if the authorisation checks fail, an error will be returned.
 * If it is not authorised, %G_DBUS_ERROR_ACCESS_DENIED will be returned.
 *
 * Since: 0.2.0
 */
void
csr_peer_manager_check_authorization_async (CsrPeerManager      *peer_manager,
                                            const gchar         *sender,
                                            GDBusConnection     *connection,
                                            const gchar         *action_id,
                                            PolkitDetails	*details,
                                            GCancellable        *cancellable,
                                            GAsyncReadyCallback  callback,
                                            gpointer             user_data)
{
  CsrPeerManagerInterface *iface;

  g_return_if_fail (CSR_IS_PEER_MANAGER (peer_manager));
  g_return_if_fail (sender != NULL && g_dbus_is_unique_name (sender));
  g_return_if_fail (G_IS_DBUS_CONNECTION (connection));
  g_return_if_fail (action_id != NULL && *action_id != '\0');

  g_return_if_fail (details == NULL || POLKIT_IS_DETAILS (details));

  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  iface = CSR_PEER_MANAGER_GET_IFACE (peer_manager);
  g_assert (iface->check_authorization_async != NULL);
  iface->check_authorization_async (peer_manager, sender, connection, action_id,
                                    details, cancellable, callback, user_data);
}

/**
 * csr_peer_manager_check_authorization_finish:
 * @peer_manager: a #CsrPeerManager
 * @result: asynchronous result
 * @error: return location for a #GError
 *
 * See csr_peer_manager_check_authorization_async().
 *
 * Since: 0.2.0
 */
void
csr_peer_manager_check_authorization_finish (CsrPeerManager  *peer_manager,
                                             GAsyncResult    *result,
                                             GError         **error)
{
  CsrPeerManagerInterface *iface;

  g_return_if_fail (CSR_IS_PEER_MANAGER (peer_manager));
  g_return_if_fail (G_IS_ASYNC_RESULT (result));
  g_return_if_fail (error == NULL || *error == NULL);

  iface = CSR_PEER_MANAGER_GET_IFACE (peer_manager);
  g_assert (iface->check_authorization_finish != NULL);
  iface->check_authorization_finish (peer_manager, result, error);
}
