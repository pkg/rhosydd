/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CSR_STATIC_VEHICLE_H
#define CSR_STATIC_VEHICLE_H

#include <glib.h>
#include <glib-object.h>

#include <libcroesor/clock.h>

G_BEGIN_DECLS

/**
 * CSR_TYPE_STATIC_VEHICLE:
 *
 * Retrieve the #CsrStaticVehicle type.
 *
 * Since: 0.2.0
 */
#define CSR_TYPE_STATIC_VEHICLE csr_static_vehicle_get_type ()
G_DECLARE_FINAL_TYPE (CsrStaticVehicle, csr_static_vehicle, CSR,
                      STATIC_VEHICLE, GObject)

CsrStaticVehicle *csr_static_vehicle_new (const gchar *id,
                                          GPtrArray   *attributes);

void              csr_static_vehicle_set_clock_func (CsrStaticVehicle *self,
                                                     CsrClockFunc      clock_func,
                                                     gpointer          user_data,
                                                     GDestroyNotify    free_func);

G_END_DECLS

#endif /* !CSR_STATIC_VEHICLE_H */
