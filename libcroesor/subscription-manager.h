/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CSR_SUBSCRIPTION_MANAGER_H
#define CSR_SUBSCRIPTION_MANAGER_H

#include <glib.h>
#include <glib-object.h>

#include <librhosydd/vehicle.h>

G_BEGIN_DECLS

/**
 * CSR_TYPE_SUBSCRIPTION_MANAGER:
 *
 * Retrieve the #CsrSubscriptionManager type.
 *
 * Since: 0.2.0
 */
#define CSR_TYPE_SUBSCRIPTION_MANAGER csr_subscription_manager_get_type ()
G_DECLARE_FINAL_TYPE (CsrSubscriptionManager, csr_subscription_manager, CSR,
                      SUBSCRIPTION_MANAGER, GObject)

CsrSubscriptionManager *csr_subscription_manager_new (void);

void csr_subscription_manager_update_vehicles      (CsrSubscriptionManager *self,
                                                    GPtrArray              *added,
                                                    GPtrArray              *removed);
void csr_subscription_manager_remove_peer          (CsrSubscriptionManager *self,
                                                    const gchar            *peer_name);
void csr_subscription_manager_update_subscriptions (CsrSubscriptionManager *self,
                                                    RsdVehicle             *vehicle,
                                                    const gchar            *peer_name,
                                                    GPtrArray              *subscriptions,
                                                    GPtrArray              *unsubscriptions);

GPtrArray *csr_subscription_manager_get_subscriptions (CsrSubscriptionManager *self,
                                                       RsdVehicle             *vehicle,
                                                       const gchar            *peer_name);

GPtrArray *csr_subscription_manager_get_peers_for_subscriptions (CsrSubscriptionManager   *self,
                                                                 RsdVehicle               *vehicle,
                                                                 GPtrArray                *changed_attributes,
                                                                 GPtrArray                *invalidated_attributes,
                                                                 RsdTimestampMicroseconds  notify_time);

G_END_DECLS

#endif /* !CSR_SUBSCRIPTION_MANAGER_H */
