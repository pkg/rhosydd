/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CSR_PEER_MANAGER_H
#define CSR_PEER_MANAGER_H

#include <glib.h>
#include <glib-object.h>
#include <polkit/polkit.h>

G_BEGIN_DECLS

/**
 * CSR_TYPE_PEER_MANAGER:
 *
 * Retrieve the #CsrPeerManager type.
 *
 * Since: 0.2.0
 */
#define CSR_TYPE_PEER_MANAGER csr_peer_manager_get_type ()

/**
 * CsrPeerManager:
 *
 * An object instance which implements #CsrPeerManagerInterface.
 *
 * Since: 0.4.0
 */
G_DECLARE_INTERFACE (CsrPeerManager, csr_peer_manager, CSR,
                     PEER_MANAGER, GObject)

/**
 * CsrPeerManagerInterface:
 * @g_iface: parent interface
 * @ensure_peer_info_async: Add a peer to the set of known peers, and look up
 *    initial information about it.
 * @ensure_peer_info_finish: Finish function for @ensure_peer_info_async.
 * @check_authorization_async: Check whether the given peer is authorized to
 *    perform a given polkit action; return an error if not.
 * @check_authorization_finish: Finish function for @check_authorization_async.
 *
 * A manager interface which manages the set of known D-Bus peer processes,
 * allowing polkit authorisation checks against them while avoiding
 * time-of-check-to-time-of-use vulnerabilities.
 *
 * Since: 0.4.0
 */
struct _CsrPeerManagerInterface
{
  GTypeInterface g_iface;

  void            (*ensure_peer_info_async)     (CsrPeerManager       *peer_manager,
                                                 const gchar          *sender,
                                                 GDBusConnection      *connection,
                                                 GCancellable         *cancellable,
                                                 GAsyncReadyCallback   callback,
                                                 gpointer              user_data);
  void   *          (*ensure_peer_info_finish)    (CsrPeerManager       *peer_manager,
                                                 GAsyncResult         *result,
                                                 GError              **error);

  void            (*check_authorization_async)  (CsrPeerManager       *peer_manager,
                                                 const gchar          *sender,
                                                 GDBusConnection      *connection,
                                                 const gchar          *action_id,
                                                 PolkitDetails	      *details,
                                                 GCancellable         *cancellable,
                                                 GAsyncReadyCallback   callback,
                                                 gpointer              user_data);

  void            (*check_authorization_finish) (CsrPeerManager       *peer_manager,
                                                 GAsyncResult         *result,
                                                 GError              **error);
};

void            csr_peer_manager_ensure_peer_info_async     (CsrPeerManager      *peer_manager,
                                                             const gchar         *sender,
                                                             GDBusConnection     *connection,
                                                             GCancellable        *cancellable,
                                                             GAsyncReadyCallback  callback,
                                                             gpointer             user_data);
void   *        csr_peer_manager_ensure_peer_info_finish    (CsrPeerManager      *peer_manager,
                                                             GAsyncResult        *result,
                                                             GError             **error);

void            csr_peer_manager_check_authorization_async  (CsrPeerManager      *peer_manager,
                                                             const gchar         *sender,
                                                             GDBusConnection     *connection,
                                                             const gchar         *action_id,
                                                             PolkitDetails	 *details,
                                                             GCancellable        *cancellable,
                                                             GAsyncReadyCallback  callback,
                                                             gpointer             user_data);

void            csr_peer_manager_check_authorization_finish (CsrPeerManager      *peer_manager,
                                                             GAsyncResult        *result,
                                                             GError             **error);

G_END_DECLS

#endif /* !CSR_PEER_MANAGER_H */
