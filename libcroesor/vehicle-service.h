/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CSR_VEHICLE_SERVICE_H
#define CSR_VEHICLE_SERVICE_H

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libcroesor/peer-manager.h>
#include <libcroesor/subscription-manager.h>
#include <libcroesor/vehicle-manager.h>

G_BEGIN_DECLS

/**
 * CSR_TYPE_VEHICLE_SERVICE:
 *
 * Retrieve the #CsrVehicleService type.
 *
 * Since: 0.2.0
 */
#define CSR_TYPE_VEHICLE_SERVICE csr_vehicle_service_get_type ()
G_DECLARE_FINAL_TYPE (CsrVehicleService, csr_vehicle_service, CSR, VEHICLE_SERVICE,
                      GObject)

CsrVehicleService *csr_vehicle_service_new        (GDBusConnection        *connection,
                                                   const gchar            *object_path,
                                                   CsrVehicleManager      *vehicle_manager,
                                                   CsrPeerManager         *peer_manager,
                                                   CsrSubscriptionManager *subscription_manager);

gboolean           csr_vehicle_service_register   (CsrVehicleService  *self,
                                                   GError            **error);
void               csr_vehicle_service_unregister (CsrVehicleService  *self);

G_END_DECLS

#endif /* !CSR_VEHICLE_SERVICE_H */
