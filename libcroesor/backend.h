/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CSR_BACKEND_H
#define CSR_BACKEND_H

#include <glib.h>
#include <glib-object.h>

#include <libcroesor/service.h>
#include <libcroesor/vehicle-manager.h>

G_BEGIN_DECLS

/**
 * CSR_BACKEND_ERROR:
 *
 * Error domain for #CsrBackendError.
 *
 * Since: 0.2.0
 */
#define CSR_BACKEND_ERROR csr_backend_error_quark ()
GQuark csr_backend_error_quark (void);

/**
 * CsrBackendError:
 * @CSR_BACKEND_ERROR_VEHICLE_UNREGISTERED: Vehicle registration denied by the
 *    service.
 *
 * Errors from running a backend.
 *
 * Since: 0.2.0
 */
typedef enum
{
  CSR_BACKEND_ERROR_VEHICLE_UNREGISTERED,
} CsrBackendError;

/**
 * CSR_TYPE_BACKEND:
 *
 * Retrieve the #CsrBackend type.
 *
 * Since: 0.2.0
 */
#define CSR_TYPE_BACKEND csr_backend_get_type ()
G_DECLARE_FINAL_TYPE (CsrBackend, csr_backend, CSR, BACKEND, CsrService)

CsrBackend *csr_backend_new (const gchar *service_id,
                             const gchar *backend_path,
                             const gchar *translation_domain,
                             const gchar *summary);

CsrVehicleManager *csr_backend_get_vehicle_manager (CsrBackend *self);

G_END_DECLS

#endif /* !CSR_BACKEND_H */
