/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CSR_VEHICLE_MANAGER_H
#define CSR_VEHICLE_MANAGER_H

#include <glib.h>
#include <glib-object.h>

#include <librhosydd/vehicle.h>

G_BEGIN_DECLS

/**
 * CSR_TYPE_VEHICLE_MANAGER:
 *
 * Retrieve the #CsrVehicleManager type.
 *
 * Since: 0.2.0
 */
#define CSR_TYPE_VEHICLE_MANAGER csr_vehicle_manager_get_type ()
G_DECLARE_FINAL_TYPE (CsrVehicleManager, csr_vehicle_manager, CSR,
                      VEHICLE_MANAGER, GObject)

CsrVehicleManager *csr_vehicle_manager_new             (void);

void               csr_vehicle_manager_update_vehicles (CsrVehicleManager *self,
                                                        GPtrArray         *added,
                                                        GPtrArray         *removed);

RsdVehicle        *csr_vehicle_manager_get_vehicle     (CsrVehicleManager *self,
                                                        const gchar       *vehicle_id);
GHashTable        *csr_vehicle_manager_get_vehicles    (CsrVehicleManager *self);

G_END_DECLS

#endif /* !CSR_VEHICLE_MANAGER_H */
