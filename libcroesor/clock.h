/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CSR_CLOCK_H
#define CSR_CLOCK_H

#include <librhosydd/types.h>
#include <glib.h>

G_BEGIN_DECLS

/**
 * SECTION:clock
 * @short_description: Clock management function
 *
 * See also #CsrClockFunc
 *
 * #clock is available since 0.4.0
 */

/**
 * CsrClockFunc:
 * @user_data: user data to pass to the clock
 *
 * A callback type which returns the value of the clock domain it represents
 * whenever it is called. The value is always in microseconds, but the clock
 * domain is not prescribed.
 *
 * Since: 0.4.0
 */
typedef RsdTimestampMicroseconds (*CsrClockFunc) (gpointer user_data);

G_END_DECLS

#endif /* !CSR_CLOCK_H */
