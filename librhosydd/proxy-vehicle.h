/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef RSD_PROXY_VEHICLE_H
#define RSD_PROXY_VEHICLE_H

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

G_BEGIN_DECLS

/**
 * RSD_TYPE_PROXY_VEHICLE:
 *
 * Get the type of a #RsdProxyVehicle.
 *
 * Since: 0.1.0
 */
#define RSD_TYPE_PROXY_VEHICLE rsd_proxy_vehicle_get_type ()
G_DECLARE_FINAL_TYPE (RsdProxyVehicle, rsd_proxy_vehicle, RSD,
                      PROXY_VEHICLE, GObject)

void             rsd_proxy_vehicle_new_async  (GDBusConnection     *connection,
                                               const gchar         *name,
                                               const gchar         *object_path,
                                               GCancellable        *cancellable,
                                               GAsyncReadyCallback  callback,
                                               gpointer             user_data);
RsdProxyVehicle *rsd_proxy_vehicle_new_finish (GAsyncResult        *result,
                                               GError             **error);

RsdProxyVehicle *rsd_proxy_vehicle_new_from_proxy (GDBusProxy  *proxy,
                                                   GError     **error);

G_END_DECLS

#endif /* !RSD_PROXY_VEHICLE_H */
