/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>

#include "libinternal/logging.h"
#include "librhosydd/vehicle-manager.h"
#include "librhosydd/proxy-vehicle.h"

/**
 * SECTION: vehicle-manager
 * @short_description: Vehicle manager object
 *
 * A manager object which communicates with the Rhosydd service to query and
 * retrieve vehicles.
 *
 * See also #RsdVehicleManager
 *
 * #vehicle-manager is available since 0.2.0
 */

static void rsd_vehicle_manager_async_initable_init (GAsyncInitableIface *iface);

static void rsd_vehicle_manager_dispose      (GObject      *object);

static void rsd_vehicle_manager_get_property (GObject      *object,
                                              guint         property_id,
                                              GValue       *value,
                                              GParamSpec   *pspec);
static void rsd_vehicle_manager_set_property (GObject      *object,
                                              guint         property_id,
                                              const GValue *value,
                                              GParamSpec   *pspec);

static void     rsd_vehicle_manager_init_async  (GAsyncInitable       *initable,
                                                 int                   io_priority,
                                                 GCancellable         *cancellable,
                                                 GAsyncReadyCallback   callback,
                                                 gpointer              user_data);
static gboolean rsd_vehicle_manager_init_finish (GAsyncInitable       *initable,
                                                 GAsyncResult         *result,
                                                 GError              **error);

/**
 * RsdVehicleManager:
 *
 * A manager object which communicates with the Rhosydd service to query and
 * retrieve vehicles.
 *
 * Since: 0.2.0
 */
struct _RsdVehicleManager
{
  GObject parent;

  GDBusConnection *connection;  /* owned */
  gchar *name;  /* well-known bus name to proxy */
  gchar *object_path;  /* root object path to proxy */
  GDBusObjectManager *client;  /* owned */
  GHashTable/*<owned utf8, owned RsdProxyVehicle>*/ *vehicles;  /* owned */

  GError *init_error;  /* nullable; owned */
  gboolean initialising;
};

typedef enum
{
  PROP_CONNECTION = 1,
  PROP_NAME,
  PROP_OBJECT_PATH,
} RsdVehicleManagerProperty;

G_DEFINE_TYPE_WITH_CODE (RsdVehicleManager, rsd_vehicle_manager, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE,
                                                rsd_vehicle_manager_async_initable_init))

static void
rsd_vehicle_manager_class_init (RsdVehicleManagerClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;
  GParamSpec *props[PROP_OBJECT_PATH + 1] = { NULL, };

  object_class->dispose = rsd_vehicle_manager_dispose;
  object_class->get_property = rsd_vehicle_manager_get_property;
  object_class->set_property = rsd_vehicle_manager_set_property;

  /**
   * RsdVehicleManager:connection:
   *
   * D-Bus connection to retrieve objects from.
   *
   * Since: 0.2.0
   */
  props[PROP_CONNECTION] =
      g_param_spec_object ("connection", "Connection",
                           "D-Bus connection to retrieve objects from.",
                           G_TYPE_DBUS_CONNECTION,
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  /**
   * RsdVehicleManager:name:
   *
   * D-Bus name of the peer to retrieve objects from. This will typically be
   * `org.apertis.Rhosydd1` for the main SDK sensors API.
   *
   * Since: 0.3.0
   */
  props[PROP_NAME] =
      g_param_spec_string ("name", "Name",
                           "D-Bus name of the peer to retrieve objects from.",
                           "org.apertis.Rhosydd1",
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  /**
   * RsdVehicleManager:object-path:
   *
   * D-Bus object path to retrieve objects from. This will typically be
   * `/org/apertis/Rhosydd1` for the main SDK sensors API.
   *
   * Since: 0.3.0
   */
  props[PROP_OBJECT_PATH] =
      g_param_spec_string ("object-path", "Object Path",
                           "D-Bus object path to retrieve objects from.",
                           "/org/apertis/Rhosydd1",
                           G_PARAM_READWRITE |
                           G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (props), props);

  /**
   * RsdVehicleManager::vehicles-changed:
   * @self: a #RsdVehicleManager
   * @added: (element-type RsdVehicle): potentially empty array of added
   *    vehicles
   * @removed: (element-type RsdVehicle): potentially empty array of removed
   *    vehicles
   *
   * Emitted when the set of vehicles known the manager changes.
   *
   * There will be at least one vehicle in one of the arrays.
   *
   * Since: 0.3.0
   */
  g_signal_new ("vehicles-changed", G_TYPE_FROM_CLASS (klass),
                G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                G_TYPE_NONE, 2, G_TYPE_PTR_ARRAY, G_TYPE_PTR_ARRAY);
}

static void
rsd_vehicle_manager_async_initable_init (GAsyncInitableIface *iface)
{
  iface->init_async = rsd_vehicle_manager_init_async;
  iface->init_finish = rsd_vehicle_manager_init_finish;
}

static void
rsd_vehicle_manager_init (RsdVehicleManager *self)
{
  /* The object path is owned by the #GDBusObject which is wrapped by the
   * #RsdProxyVehicle. */
  self->vehicles = g_hash_table_new_full (g_str_hash, g_str_equal, NULL,
                                          g_object_unref);
}

static void
rsd_vehicle_manager_dispose (GObject *object)
{
  RsdVehicleManager *self = RSD_VEHICLE_MANAGER (object);

  if (self->client != NULL)
    g_signal_handlers_disconnect_by_data (self->client, self);

  g_clear_pointer (&self->vehicles, g_hash_table_unref);
  g_clear_object (&self->connection);
  g_clear_object (&self->client);
  g_clear_error (&self->init_error);
  g_clear_pointer (&self->name, g_free);
  g_clear_pointer (&self->object_path, g_free);

  /* Chain up to the parent class */
  G_OBJECT_CLASS (rsd_vehicle_manager_parent_class)->dispose (object);
}

static void
rsd_vehicle_manager_get_property (GObject    *object,
                                  guint       property_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  RsdVehicleManager *self = RSD_VEHICLE_MANAGER (object);

  switch ((RsdVehicleManagerProperty) property_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, self->connection);
      break;
    case PROP_NAME:
      g_value_set_string (value, self->name);
      break;
    case PROP_OBJECT_PATH:
      g_value_set_string (value, self->object_path);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
rsd_vehicle_manager_set_property (GObject      *object,
                                  guint         property_id,
                                  const GValue *value,
                                  GParamSpec   *pspec)
{
  RsdVehicleManager *self = RSD_VEHICLE_MANAGER (object);

  switch ((RsdVehicleManagerProperty) property_id)
    {
    case PROP_CONNECTION:
      /* Construct only. */
      g_assert (self->connection == NULL);
      self->connection = g_value_dup_object (value);
      break;
    case PROP_NAME:
      /* Construct only. */
      g_assert (self->name == NULL);
      g_assert (g_dbus_is_name (g_value_get_string (value)));
      self->name = g_value_dup_string (value);
      break;
    case PROP_OBJECT_PATH:
      /* Construct only. */
      g_assert (self->object_path == NULL);
      g_assert (g_variant_is_object_path (g_value_get_string (value)));
      self->object_path = g_value_dup_string (value);
      break;
    default:
      g_assert_not_reached ();
    }
}

static GType
object_manager_get_proxy_type (GDBusObjectManagerClient *manager,
                               const gchar              *object_path,
                               const gchar              *interface_name,
                               gpointer                  user_data)
{
  if (interface_name == NULL)
    return G_TYPE_DBUS_OBJECT_PROXY;
  else
    return G_TYPE_DBUS_PROXY;
}

static void object_manager_new_cb (GObject      *obj,
                                   GAsyncResult *result,
                                   gpointer      user_data);

static void
rsd_vehicle_manager_init_async (GAsyncInitable      *initable,
                                int                  io_priority,
                                GCancellable        *cancellable,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
  RsdVehicleManager *self = RSD_VEHICLE_MANAGER (initable);
  g_autoptr (GTask) task = NULL;

  /* FIXME:This doesn’t support parallel initialisation. */
  g_assert (!self->initialising);

  task = g_task_new (initable, cancellable, callback, user_data);
  g_task_set_source_tag (task, rsd_vehicle_manager_init_async);

  if (self->init_error != NULL)
    g_task_return_error (task, g_error_copy (self->init_error));
  else if (self->client != NULL)
    g_task_return_boolean (task, TRUE);
  else
    {
      self->initialising = TRUE;
      g_dbus_object_manager_client_new (self->connection,
                                        G_DBUS_OBJECT_MANAGER_CLIENT_FLAGS_NONE,
                                        self->name,
                                        self->object_path,
                                        object_manager_get_proxy_type,
                                        NULL, NULL,
                                        cancellable,
                                        object_manager_new_cb,
                                        g_object_ref (task));
    }
}

static void object_added_cb   (GDBusObjectManager *manager,
                               GDBusObject        *object,
                               gpointer            user_data);
static void object_removed_cb (GDBusObjectManager *manager,
                               GDBusObject        *object,
                               gpointer            user_data);

static void
object_manager_new_cb (GObject      *obj,
                       GAsyncResult *result,
                       gpointer      user_data)
{
  RsdVehicleManager *self;
  g_autoptr (GTask) task = G_TASK (user_data);
  GError *error = NULL;

  self = g_task_get_source_object (task);
  self->client = g_dbus_object_manager_client_new_finish (result, &error);

  if (self->client != NULL)
    {
      g_signal_connect (self->client, "object-added",
                        (GCallback) object_added_cb, self);
      g_signal_connect (self->client, "object-removed",
                        (GCallback) object_removed_cb, self);
    }

  g_assert (self->initialising);
  self->initialising = FALSE;

  if (error != NULL)
    {
      self->init_error = g_error_copy (error);
      g_task_return_error (task, error);
    }
  else
    {
      g_task_return_boolean (task, TRUE);
    }
}

static gboolean
rsd_vehicle_manager_init_finish (GAsyncInitable  *initable,
                                 GAsyncResult    *result,
                                 GError         **error)
{
  return g_task_propagate_boolean (G_TASK (result), error);
}

/* transfer full */
static RsdVehicle *
get_vehicle (RsdVehicleManager *self,
             GDBusObject       *object)
{
  RsdProxyVehicle *cached_vehicle;
  g_autoptr (RsdProxyVehicle) vehicle = NULL;
  g_autoptr (GDBusProxy) proxy = NULL;
  g_autoptr (GError) error = NULL;

  /* See if it exists in the cache first. */
  cached_vehicle = g_hash_table_lookup (self->vehicles,
                                        (gpointer) g_dbus_object_get_object_path (object));

  if (cached_vehicle != NULL)
    {
      DEBUG ("Found vehicle ‘%s’ in the cache.",
             g_dbus_object_get_object_path (object));
      return RSD_VEHICLE (g_object_ref (cached_vehicle));
    }

  proxy = G_DBUS_PROXY (g_dbus_object_get_interface (object,
                                                     "org.apertis.Rhosydd1.Vehicle"));
  if (proxy == NULL)
    return NULL;

  vehicle = rsd_proxy_vehicle_new_from_proxy (proxy, &error);
  if (error != NULL)
    {
      WARNING ("Error creating proxy for vehicle ‘%s’: %s",
               g_dbus_object_get_object_path (object), error->message);
      return NULL;
    }

  DEBUG ("Adding vehicle ‘%s’ (‘%s’) to the cache.",
         g_dbus_object_get_object_path (object),
         rsd_vehicle_get_id (RSD_VEHICLE (vehicle)));
  g_hash_table_replace (self->vehicles,
                        (gpointer) g_dbus_object_get_object_path (object),
                        g_object_ref (vehicle));

  return RSD_VEHICLE (g_steal_pointer (&vehicle));
}

static void
signal_object_added_or_removed (RsdVehicleManager *self,
                                GDBusObject       *object,
                                gboolean           is_added)
{
  g_autoptr (GPtrArray/*<owned RsdVehicle>*/) added = NULL;
  g_autoptr (GPtrArray/*<owned RsdVehicle>*/) removed = NULL;
  g_autoptr (RsdVehicle) vehicle = NULL;

  vehicle = get_vehicle (self, object);

  if (vehicle != NULL)
    {
      added = g_ptr_array_new_with_free_func (g_object_unref);
      removed = g_ptr_array_new_with_free_func (g_object_unref);

      if (is_added)
        {
          DEBUG ("Adding vehicle ‘%s’.", rsd_vehicle_get_id (vehicle));
          g_ptr_array_add (added, g_steal_pointer (&vehicle));
        }
      else
        {
          DEBUG ("Removing vehicle ‘%s’.", rsd_vehicle_get_id (vehicle));
          g_ptr_array_add (removed, g_steal_pointer (&vehicle));
        }

      g_signal_emit_by_name (G_OBJECT (self), "vehicles-changed",
                             added, removed);
    }
}

static void
object_added_cb (GDBusObjectManager *manager,
                 GDBusObject        *object,
                 gpointer            user_data)
{
  /* Don’t add it to the cache — we do that lazily when it’s accessed. */
  signal_object_added_or_removed (RSD_VEHICLE_MANAGER (user_data), object,
                                  TRUE);
}

static void
object_removed_cb (GDBusObjectManager *manager,
                   GDBusObject        *object,
                   gpointer            user_data)
{
  RsdVehicleManager *self = RSD_VEHICLE_MANAGER (user_data);

  /* Remove from the cache. */
  DEBUG ("Removing vehicle ‘%s’ from the cache.",
         g_dbus_object_get_object_path (object));
  g_hash_table_remove (self->vehicles,
                       (gpointer) g_dbus_object_get_object_path (object));

  signal_object_added_or_removed (RSD_VEHICLE_MANAGER (user_data), object,
                                  FALSE);
}

/**
 * rsd_vehicle_manager_new_async:
 * @connection: D-Bus connection to use
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to invoke on completion
 * @user_data: user data to pass to @callback
 *
 * Create a new #RsdVehicleManager on @connection, and set up the proxy object
 * to proxy vehicles from the SDK API.
 *
 * This is an asynchronous process which might fail; object instantiation must
 * be finished (or the error returned) by calling
 * rsd_vehicle_manager_new_finish().
 *
 * Since: 0.2.0
 */
void
rsd_vehicle_manager_new_async (GDBusConnection     *connection,
                               GCancellable        *cancellable,
                               GAsyncReadyCallback  callback,
                               gpointer             user_data)
{
  g_return_if_fail (G_IS_DBUS_CONNECTION (connection));
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  rsd_vehicle_manager_new_for_name_async (connection, "org.apertis.Rhosydd1",
                                          "/org/apertis/Rhosydd1", cancellable,
                                          callback, user_data);
}

/**
 * rsd_vehicle_manager_new_finish:
 * @result: asynchronous operation result
 * @error: return location for a #GError
 *
 * Finish initialising a #RsdVehicleManager. See rsd_vehicle_manager_new_async().
 *
 * Returns: (transfer full): initialised #RsdVehicleManager, or %NULL on error
 * Since: 0.2.0
 */
RsdVehicleManager *
rsd_vehicle_manager_new_finish (GAsyncResult       *result,
                                GError            **error)
{
  return rsd_vehicle_manager_new_for_name_finish (result, error);
}

/**
 * rsd_vehicle_manager_new_for_name_async:
 * @connection: D-Bus connection to use
 * @name: D-Bus name to connect to and retrieve vehicles from
 * @object_path: root of the object manager on @name
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to invoke on completion
 * @user_data: user data to pass to @callback
 *
 * Create a new #RsdVehicleManager on @connection, proxying vehicles from
 * beneath @object_path on the given @name, and set up the proxy object.
 *
 * This is an asynchronous process which might fail; object instantiation must
 * be finished (or the error returned) by calling
 * rsd_vehicle_manager_new_finish().
 *
 * Since: 0.3.0
 */
void
rsd_vehicle_manager_new_for_name_async (GDBusConnection     *connection,
                                        const gchar         *name,
                                        const gchar         *object_path,
                                        GCancellable        *cancellable,
                                        GAsyncReadyCallback  callback,
                                        gpointer             user_data)
{
  g_return_if_fail (G_IS_DBUS_CONNECTION (connection));
  g_return_if_fail (g_dbus_is_name (name));
  g_return_if_fail (g_variant_is_object_path (object_path));
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  g_async_initable_new_async (RSD_TYPE_VEHICLE_MANAGER, G_PRIORITY_DEFAULT,
                              cancellable, callback, user_data,
                              "connection", connection,
                              "name", name,
                              "object-path", object_path,
                              NULL);
}

/**
 * rsd_vehicle_manager_new_for_name_finish:
 * @result: asynchronous operation result
 * @error: return location for a #GError
 *
 * Finish initialising a #RsdVehicleManager. See
 * rsd_vehicle_manager_new_for_name_async().
 *
 * Returns: (transfer full): initialised #RsdVehicleManager, or %NULL on error
 * Since: 0.3.0
 */
RsdVehicleManager *
rsd_vehicle_manager_new_for_name_finish (GAsyncResult       *result,
                                         GError            **error)
{
  g_autoptr (GObject) source_object = NULL;

  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  source_object = g_async_result_get_source_object (result);
  return RSD_VEHICLE_MANAGER (g_async_initable_new_finish (G_ASYNC_INITABLE (source_object),
                                                           result, error));
}

/**
 * rsd_vehicle_manager_list_vehicles:
 * @self: a #RsdVehicleManager
 *
 * List the vehicles known to the vehicle manager.
 *
 * Returns: (transfer container) (element-type RsdVehicle): potentially empty array
 *    of vehicles
 * Since: 0.2.0
 */
GPtrArray *
rsd_vehicle_manager_list_vehicles (RsdVehicleManager *self)
{
  g_autoptr (GPtrArray/*<owned RsdProxyVehicle>*/) vehicles = NULL;
  GList/*<owned GDBusObject>*/ *proxies = NULL, *l;

  g_return_val_if_fail (RSD_IS_VEHICLE_MANAGER (self), NULL);

  vehicles = g_ptr_array_new_with_free_func (g_object_unref);
  proxies = g_dbus_object_manager_get_objects (self->client);

  for (l = proxies; l != NULL; l = l->next)
    {
      GDBusObject *obj = G_DBUS_OBJECT (l->data);
      g_autoptr (RsdVehicle) vehicle = NULL;

      vehicle = get_vehicle (self, obj);

      if (vehicle != NULL)
        g_ptr_array_add (vehicles, g_steal_pointer (&vehicle));
    }

  g_list_free_full (proxies, g_object_unref);

  return g_steal_pointer (&vehicles);
}

/**
 * rsd_vehicle_manager_get_vehicle:
 * @self: an #RsdVehicleManager
 * @vehicle_id: ID of the vehicle to retrieve
 *
 * Get a particular vehicle from the vehicle manager. If the vehicle is unknown,
 * %NULL is returned.
 *
 * Returns: (transfer full) (nullable): the vehicle
 * Since: 0.2.0
 */
RsdVehicle *
rsd_vehicle_manager_get_vehicle (RsdVehicleManager *self,
                                 const gchar       *vehicle_id)
{
  g_autofree gchar *object_path = NULL;
  g_autoptr (GDBusObject) obj = NULL;

  g_return_val_if_fail (RSD_IS_VEHICLE_MANAGER (self), NULL);
  g_return_val_if_fail (rsd_vehicle_id_is_valid (vehicle_id), NULL);

  if (g_strcmp0 (self->object_path, "/") == 0)
    object_path = g_strdup_printf ("/%s", vehicle_id);
  else
    object_path = g_strdup_printf ("%s/%s", self->object_path, vehicle_id);

  obj = g_dbus_object_manager_get_object (self->client, object_path);

  if (obj == NULL)
    return NULL;

  return get_vehicle (self, obj);
}

/**
 * rsd_vehicle_manager_get_name:
 * @self: an #RsdVehicleManager
 *
 * Get the value of #RsdVehicleManager:name.
 *
 * Returns: D-Bus name of the peer to retrieve objects from
 * Since: 0.3.0
 */
const gchar *
rsd_vehicle_manager_get_name (RsdVehicleManager *self)
{
  g_return_val_if_fail (RSD_IS_VEHICLE_MANAGER (self), NULL);

  return self->name;
}
