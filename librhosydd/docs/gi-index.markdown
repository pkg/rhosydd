# API reference

## librhosydd Overview

[Mail the maintainers](mailto:rhosydd@apertis.org)

librhosydd is a library which provides applications access to the sensors and
actuators attached to a vehicle. It is an interface to the system’s sensors
and actuators daemon, which it communicates with over D-Bus. librhosydd is
intended to be used by applications and system services.

The overall design of the sensors and actuators system is given in the
[Sensors and Actuators design document](https://wiki.apertis.org/mediawiki/index.php/ConceptDesigns).
The design of the system daemon is discussed in the documentation for
libcroesor, a library used in the implementation of the service and its
backends.

The main interface for librhosydd is the vehicle manager ([RsdVehicleManager]())
which provides a list of the available vehicles. An [RsdVehicle](), as returned
by the manager, represents a physical vehicle, and there might be multiple
vehicles connected to the system. For example, if a car is towing a trailer, and
has a removable roof rack attached, the manager might list three vehicles:
 * Car
 * Trailer
 * Roof rack

Each vehicle has one or more zones, and zero or more attributes. Each attribute
is in a zone, and duplicate attributes are only allowed if they are in different
zones. The zones for a vehicle are unique to that vehicle, and form a tree which
can be used to give the physical location of an attribute on the vehicle.

For example, a car might have a `tire.pressure` attribute, which is present in
four different zones: the front–left, front–right, rear–left and rear–right
zones. These four attribute values give the pressures for all tyres.

Zones are nested in a hierarchy, so if (for example) an attribute needed to be
presented for the flow rate of two air conditioning vents in front of the front
passenger seat, they could be put in zones:
 * `/front,left/left/`
 * `/front,left/right/`
And the foot vent could be in zone:
 * `/front,left/bottom/`
In this example, the ‘path’ syntax for representing zones has been used, which
presents the zone hierarchy unambiguously and compactly. It is described in
detail in the [zone paths documentation]{librhosydd-api.markdown#zone-paths}.

Attributes can be readable, writable, or unavailable (for a variety of reasons).
An attribute which is read-only is known as a sensor, as it will typically
correspond to a physical sensor in the vehicle. Similarly, an attribute which is
writable is known as an actuator. Attributes may be unavailable due to security
policies imposed by the application framework, or by the vehicle OEM; meaning
that applications cannot access them at all. On the other hand, they may also
be unavailable because of some limitation on the physical device they are
associated with — for example, heated seat controls might be disabled for the
rear seats in a car if they are folded down to extend the boot space. Or they
may correspond to a feature which is only available in a higher-specification
model of the same family of vehicles.

An application’s access to attributes is subject to security policy imposed by
various parts of the system, including the application’s manifest. Permission to
//enumerate// attributes is separate from permission to //access// them, which
means that an application which is allowed to enumerate attributes may get the
metadata for all attributes, but cannot necessarily get their values. Metadata
for attributes covers their readability and writability, and their availability.

Applications can be notified of changes to attribute metadata and values by
connecting to the [#RsdVehicle::attributes-metadata-changed]() or
[#RsdVehicle::attributes-changed]() signals. First, an application must
//subscribe// to receive updates for the attributes it is interested in, using
rsd_vehicle_update_subscriptions_async(). To receive updates for all attributes
which the application is authorised to see, use a wildcard subscription
(rsd_subscription_new_wildcard()).

Note that the system may notify the application of attributes other than those
it is subscribed to, if doing so is more efficient than removing those
attributes from a signal emission. Applications should handle, but not rely on,
this behaviour.

Each attribute value has an update timestamp associated with it, which is set by
the backend at the same time as the value is set. Ideally, the timestamp gives
the time when that value was measured by the sensor; but due to limitations in
the automotive domain, it may give a later time, such as when the value was read
off the vehicle’s CAN bus. As long as the timestamps for all attribute values are
consistent, and come from the same clock, they are useful. The intention with
providing attribute timestamps is to allow applications to factor them into
predictions or models of how the value changes over time.

The clock domains for two vehicles may differ, as long as all the timestamps for
the attributes within a single vehicle are comparable with each other. Hence it
may not be possible to compare the timestamps for two attributes from different
vehicles.

As discussed in the design document, librhosydd and the system’s sensors and
actuators daemon are not designed for transmitting high bandwidth or low latency
sensor data. It should be transmitted out of band, using separate channels which
are set up via librhosydd (for example, a Unix socket which is advertised over
D-Bus).

## Zones

The W3C Vehicle Information Access API has a [concept of
‘zones’](http://www.w3.org/2014/automotive/vehicle_spec.html#zone-interface)
which indicate the physical location of a device in the vehicle. The current
version of the specification has a misleading `ZonePosition` enumerated type which
is not used elsewhere in the API. The zones which apply to an attribute are
specified as an array of opaque strings, which may have values other than those
in `ZonePosition`. Multiple strings can be used (like tags) to describe the
location of a device in several dimensions. Furthermore, zones may be nested
hierarchically to build an abstract map of the layout of a vehicle.

Each zone has a unique string path (its zone path), a parent zone, and a
(potentially empty) list of tags to differentiate it from its siblings. There is
always a root zone (path `/`, no tags) which represents the entire
vehicle. Each tree of zones is unique to a particular vehicle. Each zone is
uniquely identified within this tree by its path, which is the combination of
its parent path and tag list. Consequently, no two siblings may have the same
tag list. However, they may share entries in their tag lists, which allows
‘overlapping’ areas in the vehicle to be represented.

`ZonePosition` has been extended with additional strings to better describe
attribute locations. Strings which are not defined in this extended enumerated
type must not be used in [](RsdZone).

Attributes should be tagged with zone information which is likely to be useful to
application developers. For example, it is typically not useful to know whether
the engine is in the front or rear of the vehicle, but is useful to know that a
particular light is an interior light, above the driver.

## Zone Paths

Zone paths are formed of tag lists, separated by slashes. Each tag list is a
potentially empty list of tags, separated by commas, in alphabetical order. Each
tag must be a non-empty string from the letters a-z, A-Z and the digits 0-9.

Zone paths are *not* D-Bus object paths, and must not be confused with them,
even though they use similar syntax.

A zone path must start and end with a slash: the leading slash indicates the
root zone, and each tag list is followed by a slash. So the root zone has the
path `/`. A child zone of the root zone which has an empty tag list has path
`//`. A child zone of the root zone which is tagged `left` has path `/left/`.
One which is tagged `top` and `left` has path `/left,top/`. A lower descendant
of the root zone might be `/left,top//center/front/`.

As a special case, the parent zone of the root zone is the empty string. This is
not a valid zone path, but is accepted in certain situations (where documented).

## Attribute Names

Attributes are named following the [Vehicle Data
specification](https://www.w3.org/2014/automotive/data_spec.html), starting with
the [Vehicle
interface](https://www.w3.org/2014/automotive/vehicle_spec.html#idl-def-Vehicle).

Different parts of the specification add partial interfaces which extend the
Vehicle interface. For example, fuel configuration information is exposed as
attributes starting with
[`fuelConfiguration`](https://www.w3.org/2014/automotive/data_spec.html#idl-def-Vehicle):
 * [`fuelConfiguration.fuelType`](https://www.w3.org/2014/automotive/data_spec.html#idl-def-FuelConfiguration)
 * `fuelConfiguration.refuelPosition`

Attribute names are formed of components (which may contain the letters a-z,
A-Z, and the digits 0-9; they start with a letter a-z, and are in camelCase; or
start with a letter A-Z and are in capitals) separated by dots. Attribute names
start and end with a component (not a dot) and contain one or more components.

Custom (non-standardised) attributes are exposed beneath an OEM-specific
namespace, using reverse-DNS notation for a domain which is controlled by that
OEM. For example, for a vendor ‘My OEM’ whose website is myoem.com, the
following attributes might be used:
 * `com.myoem.fancySeatController.backTemperature`
 * `com.myoem.roofRack.open`
 * `com.myoem.roofRack.mass`

## Vehicle IDs

Each vehicle has an identifier, and if attributes from two backends are to be
aggregated, the backends must expose them using the same vehicle ID. It is
assumed that the backends acquire the vehicle ID through an out-of-band
mechanism.

Vehicle IDs are as described in [](rsd_vehicle_id_is_valid).
