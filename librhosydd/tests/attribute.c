/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <locale.h>
#include <string.h>

#include "librhosydd/attribute.h"


/* Test that attribute construction works. */
static void
test_attribute_construction (void)
{
  g_autoptr (RsdAttribute) attribute = NULL;

  attribute = rsd_attribute_new (g_variant_new_string ("hello"),
                                 0.0,
                                 RSD_TIMESTAMP_UNKNOWN);

  g_assert_nonnull (attribute->value);
  g_assert_cmpstr ((const gchar *) g_variant_get_type (attribute->value), ==,
                   (const gchar *) G_VARIANT_TYPE_STRING);
  g_assert_cmpfloat (attribute->accuracy, ==, 0.0);
  g_assert_cmpuint (attribute->last_updated, ==, RSD_TIMESTAMP_UNKNOWN);
}

static void
assert_cmp_attribute (const RsdAttribute *attribute1,
                      const RsdAttribute *attribute2)
{
  g_assert_true (g_variant_equal (attribute1->value, attribute2->value));
  g_assert_cmpfloat (attribute1->accuracy, ==, attribute2->accuracy);
  g_assert_cmpuint (attribute1->last_updated, ==, attribute2->last_updated);
}

/* Test that copying an attribute works. */
static void
test_attribute_copy (void)
{
  g_autoptr (RsdAttribute) attribute1 = NULL, attribute2 = NULL;

  attribute1 = rsd_attribute_new (g_variant_new_string ("hello"),
                                 0.0,
                                 RSD_TIMESTAMP_UNKNOWN);
  attribute2 = rsd_attribute_copy (attribute1);

  assert_cmp_attribute (attribute2, attribute1);
}

/* Test that initialising a stack-allocated attribute works. */
static void
test_attribute_init (void)
{
  g_autoptr (RsdAttribute) attribute1 = NULL;
  g_auto (RsdAttribute) attribute2;

  attribute1 = rsd_attribute_new (g_variant_new_string ("hello"),
                                 0.0,
                                 RSD_TIMESTAMP_UNKNOWN);
  rsd_attribute_init (&attribute2, attribute1);

  assert_cmp_attribute (&attribute2, attribute1);
}

/* Test that invalid attribute names are rejected and valid ones are accepted */
static void
test_attribute_name_validation (void)
{
  const gchar *valid_names[] = {
    "drivingMode.mode",
    "identifier.VIN",
    "identifier.VIN1",
    "fuelConfiguration.fuelType",
    "fuelConfiguration.refuelPosition",
    "singleComponent",
    "Signal.Speed",
  };
  const gchar *invalid_names[] = {
    NULL,
    "",
    " ",
    "drivingMode.mode!",
    "wrong_separator",
    "identifier.0startswithdigit",
    "identifier.0STARTSWITHDIGIT",
    "dual..separator",
    ".",
    "endsWithSeparator.",
    ".startsWithSeparator",
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (valid_names); i++)
    {
      g_test_message ("Valid name %" G_GSIZE_FORMAT ": %s", i, valid_names[i]);
      g_assert_true (rsd_attribute_name_is_valid (valid_names[i]));
    }

  for (i = 0; i < G_N_ELEMENTS (invalid_names); i++)
    {
      g_test_message ("Invalid name %" G_GSIZE_FORMAT ": %s", i,
                      invalid_names[i]);
      g_assert_false (rsd_attribute_name_is_valid (invalid_names[i]));
    }
}

/* Test that invalid availabilities and rejected and valid ones are accepted. */
static void
test_attribute_availability_validation (void)
{
  gint i;

  for (i = -1; i < 6; i++)
    {
      gboolean valid = rsd_attribute_availability_is_valid (i);

      g_test_message ("Availability %i", i);
      if (i < 0 || i > 5)
        g_assert_false (valid);
      else
        g_assert_true (valid);
    }
}

/* Test that invalid flags are rejected and valid ones are accepted. */
static void
test_attribute_flags_validation (void)
{
  guint i;
  guint32 flags;
  const RsdAttributeFlags flags_mask =
      RSD_ATTRIBUTE_READABLE | RSD_ATTRIBUTE_WRITABLE;

  /* Try each flag individually. */
  for (i = 0; i < 32; i++)
    {
      flags = (1 << i);
      g_test_message ("Flags %u", flags);

      if (flags & flags_mask)
        g_assert_true (rsd_attribute_flags_is_valid (flags));
      else
        g_assert_false (rsd_attribute_flags_is_valid (flags));
    }

  /* Try none and all of them. */
  g_assert_true (rsd_attribute_flags_is_valid (RSD_ATTRIBUTE_FLAGS_NONE));
  g_assert_true (rsd_attribute_flags_is_valid (flags_mask));
}

static void
assert_is_interned (const gchar *str)
{
  g_assert (str == g_intern_string (str));
}

/* Test that attribute metadata construction works. */
static void
test_attribute_metadata_construction (void)
{
  GVariantBuilder builder;
  GVariant *metainfo = NULL;
  g_autoptr (RsdAttributeMetadata) metadata = NULL;

  g_variant_builder_init(&builder, G_VARIANT_TYPE("a{sv}"));

  g_variant_builder_add(&builder, "{sv}", "type",
             g_variant_new_string("sensor"));

  g_variant_builder_add(&builder, "{sv}", "unit",
              g_variant_new_string("kmph"));

  g_variant_builder_add(&builder, "{sv}", "datatype",
              g_variant_new_string("uint16"));

  metainfo = g_variant_ref(g_variant_builder_end(&builder));

  metadata = rsd_attribute_metadata_new ("someAttribute",
                                         RSD_ATTRIBUTE_AVAILABLE,
                                         RSD_ATTRIBUTE_READABLE,
                                         metainfo);

  g_assert_cmpstr (metadata->name, ==, "someAttribute");
  assert_is_interned (metadata->name);

  g_assert_cmpuint (metadata->availability, ==, RSD_ATTRIBUTE_AVAILABLE);
  g_assert_cmpuint (metadata->flags, ==, RSD_ATTRIBUTE_READABLE);
}

static void
assert_cmp_attribute_metadata (const RsdAttributeMetadata *metadata1,
                              const RsdAttributeMetadata *metadata2)
{
  /* Should all be interned, and hence have equal pointers. */
  g_assert (metadata1->name == metadata2->name);

  g_assert_cmpuint (metadata1->availability, ==, metadata2->availability);
  g_assert_cmpuint (metadata1->flags, ==, metadata2->flags);
}

/* Test that copying an attribute metadata works. */
static void
test_attribute_metadata_copy (void)
{
  GVariantBuilder builder;
  GVariant *metainfo;
  g_autoptr (RsdAttributeMetadata) metadata1 = NULL, metadata2 = NULL;

  g_variant_builder_init(&builder, G_VARIANT_TYPE("a{sv}"));

  g_variant_builder_add(&builder, "{sv}", "type",
             g_variant_new_string("attribute"));

  g_variant_builder_add(&builder, "{sv}", "datatype",
              g_variant_new_string("boolean"));

  metainfo = g_variant_ref(g_variant_builder_end(&builder));

  metadata1 = rsd_attribute_metadata_new ("someAttribute",
                                          RSD_ATTRIBUTE_AVAILABLE,
                                          RSD_ATTRIBUTE_READABLE,  
                                          metainfo ); 

  metadata2 = rsd_attribute_metadata_copy (metadata1);

  assert_cmp_attribute_metadata (metadata2, metadata1);
}

/* Test that initialising a stack-allocated attribute metadata works. */
static void
test_attribute_metadata_init (void)
{
  GVariantBuilder builder;
  GVariant *metainfo = NULL;
  g_autoptr (RsdAttributeMetadata) metadata1 = NULL;
  RsdAttributeMetadata metadata2;

  g_variant_builder_init(&builder, G_VARIANT_TYPE("a{sv}"));

  g_variant_builder_add(&builder, "{sv}", "unit",
             g_variant_new_string("s"));

  g_variant_builder_add(&builder, "{sv}", "min",
              g_variant_new_uint32(10));

  metainfo = g_variant_ref(g_variant_builder_end(&builder));

  metadata1 = rsd_attribute_metadata_new ("someAttribute",
                                          RSD_ATTRIBUTE_NOT_SUPPORTED_YET,
                                          RSD_ATTRIBUTE_READABLE,
                                          metainfo ); 

  rsd_attribute_metadata_init (&metadata2, metadata1);

  assert_cmp_attribute_metadata (&metadata2, metadata1);
}

/* Test that clearing a stack-allocated attribute metadata works. */
static void
test_attribute_metadata_clear (void)
{
  RsdAttributeMetadata metadata;

  rsd_attribute_metadata_init (&metadata, NULL);

  g_assert_null (metadata.name);
  g_assert_cmpuint (metadata.availability, ==, RSD_ATTRIBUTE_NOT_SUPPORTED);
  g_assert_cmpuint (metadata.flags, ==, RSD_ATTRIBUTE_FLAGS_NONE);
}

/* Test that attribute info construction works. */
static void
test_attribute_info_construction (void)
{
  GVariant  *metainfo = NULL;
  GVariantBuilder builder;
  g_autoptr (RsdAttribute) attribute = NULL;
  g_autoptr (RsdAttributeMetadata) metadata = NULL;
  g_autoptr (RsdAttributeInfo) info = NULL;

  g_variant_builder_init (&builder, G_VARIANT_TYPE("a{sv}"));

  g_variant_builder_add (&builder, "{sv}", "type",
             g_variant_new_string("attribute"));

  g_variant_builder_add (&builder, "{sv}", "max",
              g_variant_new_uint16 (80));

  metainfo = g_variant_ref (g_variant_builder_end(&builder));

  attribute = rsd_attribute_new (g_variant_new_string ("hello"),
                                 0.0,
                                 RSD_TIMESTAMP_UNKNOWN);

  metadata = rsd_attribute_metadata_new ( "someAttribute",
                                          RSD_ATTRIBUTE_AVAILABLE,
                                          RSD_ATTRIBUTE_READABLE,
                                          metainfo );

  info = rsd_attribute_info_new (attribute, metadata);

  assert_cmp_attribute (&info->attribute, attribute);
  assert_cmp_attribute_metadata (&info->metadata, metadata);
}

static void
assert_cmp_attribute_info (const RsdAttributeInfo *info1,
                           const RsdAttributeInfo *info2)
{
  assert_cmp_attribute (&info1->attribute, &info2->attribute);
  assert_cmp_attribute_metadata (&info1->metadata, &info2->metadata);
}

/* Test that copying an attribute info works. */
static void
test_attribute_info_copy (void)
{
  GVariantBuilder builder;
  GVariant *metainfo = NULL;
  g_autoptr (RsdAttribute) attribute = NULL;
  g_autoptr (RsdAttributeMetadata) metadata = NULL;
  g_autoptr (RsdAttributeInfo) info1 = NULL, info2 = NULL;

  attribute = rsd_attribute_new (g_variant_new_string ("hello"),
                                 0.0,
                                 RSD_TIMESTAMP_UNKNOWN);

  g_variant_builder_init(&builder, G_VARIANT_TYPE("a{sv}"));

  g_variant_builder_add(&builder, "{sv}", "type",
             g_variant_new_string ("attribute"));

  g_variant_builder_add(&builder, "{sv}", "unit",
              g_variant_new_string("cm/s"));

  metainfo = g_variant_ref(g_variant_builder_end(&builder));

  metadata = rsd_attribute_metadata_new ( "someAttribute",
                                          RSD_ATTRIBUTE_AVAILABLE,
                                          RSD_ATTRIBUTE_WRITABLE,
                                          metainfo );

  info1 = rsd_attribute_info_new (attribute, metadata);

  info2 = rsd_attribute_info_copy (info1);

  assert_cmp_attribute_info (info2, info1);
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/attribute/construction", test_attribute_construction);
  g_test_add_func ("/attribute/copy", test_attribute_copy);
  g_test_add_func ("/attribute/init", test_attribute_init);

  g_test_add_func ("/attribute/name/validation", test_attribute_name_validation);
  g_test_add_func ("/attribute/availability/validation",
                   test_attribute_availability_validation);
  g_test_add_func ("/attribute/flags/validation",
                   test_attribute_flags_validation);

  g_test_add_func ("/attribute/metadata/construction",
                   test_attribute_metadata_construction);
  g_test_add_func ("/attribute/metadata/copy", test_attribute_metadata_copy);
  g_test_add_func ("/attribute/metadata/init", test_attribute_metadata_init);
  g_test_add_func ("/attribute/metadata/clear", test_attribute_metadata_clear);

  g_test_add_func ("/attribute/info/construction",
                   test_attribute_info_construction);
  g_test_add_func ("/attribute/info/copy", test_attribute_info_copy);

  return g_test_run ();
}
