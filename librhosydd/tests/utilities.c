/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <locale.h>

#include "librhosydd/subscription.h"
#include "librhosydd/utilities.h"
#include "librhosydd/vehicle.h"

/* Test that rsd_attribute_info_array_[to|from]_variant() works to round-trip
 * between variant and GPtrArray forms of RsdAttributeInfo arrays. */
static void
test_utilities_attribute_info_array_valid (void)
{
  /* These must all be GVariants in text format, of type a(s(vdx)a{sv}). */
  const gchar *vectors[] = {
    "[]",
    "[('testAttribute', (<15>, 0.1, 0), [{'type', <'sensor'>}],(0, 0))]",
    "[('testAttribute', (<15>, 0.1, 0), [{'type', <'attribute'>}], (0, 0)),"
     "('testAttribute2', (<15>, 0.1, 0), [{'unit', <'km/h'>}],(0, 0))]",
    "[('testAttributeAvailable', (<15>, 0.1, 0), [{'datatype', <'bool'>}], (1, 0))]",
    "[('testAttributeNotSupportedYet', (<15>, 0.1, 0), [], (2, 0))]",
    "[('testAttributeNotSupportedSecurityPolicy', (<15>, 0.1, 0), [], (3, 0))]",
    "[('testAttributeNotSupportedBusinessPolicy', (<15>, 0.1, 0), [], (4, 0))]",
    "[('testAttributeNotSupportedOther', (<15>, 0.1, 0), [], (5, 0))]",
    "[('testAttributeReadable', (<15>, 0.1, 0), [], (0, 1))]",
    "[('testAttributeWritable', (<15>, 0.1, 0), [], (0, 2))]",
    "[('testAttributeReadWrite', (<15>, 0.1, 0) ,[], (0, 3))]",
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (GVariant) variant = NULL, variant2 = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) array = NULL;

      g_test_message ("Vector %" G_GSIZE_FORMAT ": %s", i, vectors[i]);

      /* Sanity check and parse the test data. */
      variant = g_variant_parse (G_VARIANT_TYPE ("a(s(vdx)a{sv}(uu))"),
                                 vectors[i], NULL, NULL, &error);
      g_assert_no_error (error);

      /* Convert to an array. */
      array = rsd_attribute_info_array_from_variant (variant,
                                                     0  /* current time */,
                                                     &error);
      g_assert_no_error (error);
      g_assert_nonnull (array);

      /* Complete the round trip to the variant. */
      variant2 = rsd_attribute_info_array_to_variant (array);
      g_assert (g_variant_equal (variant, variant2));
    }
}

/* Test that rsd_attribute_info_array_from_variant() throws appropriate errors
 * when given an invalid variant. */
static void
test_utilities_attribute_info_array_from_variant_error (void)
{
  const struct {
    const gchar *variant;
    RsdVehicleError expected_error_code;
  } vectors[] = {
    { "'not an array'", RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "['invalid child']", RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "[( 'invalid attribute name', (<15>, 0.1, @x 0),@a{sv} [], (@u 0,@u 0))]",
      RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE },
    { "[('testAttribute', ('invalid value', 0.1, @x 0), [{'unit', <'km/h'>}, {'min', <50>}],"
       "(@u 0, @u 0))]", RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "[('testAttribute', (<15>, -1000.5, @x 0), [{'type', <'attribute'>}],"
       "(@u 0, @u 0))]", RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "[('testAttribute', (<15>, 0.1, @x 0), @a{sv} [], (@u 6, @u 0))]", // invalid availability 
      RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "[('testAttribute', (<15>, 0.1, @x 0), @a{sv} [], (@u 0, @u 5))]", // invalid flags
      RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "[('testAttribute', (<15>, 0.1, @x 0), @a{sv} [], (@u 0, @u 0)),"
       "('testAttribute', (<16>, 0.1, @x 0), [{'datatype', <'int64'>}], (@u 0, @u 0))]",
      RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },  // duplicate attribute/zone pair 
    { "[('testAttribute', (<15>, 0.1, @x 1), @a{sv} [], (@u 0, @u 0))]", // last_updated > current_time 
      RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },

  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (GVariant) variant = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) array = NULL;

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expected error %u): %s", i,
                      vectors[i].expected_error_code, vectors[i].variant);

      /* Sanity check and parse the test data. */
      variant = g_variant_parse (NULL, vectors[i].variant, NULL, NULL, &error);
      g_assert_no_error (error);
      /* Try parsing. */
      array = rsd_attribute_info_array_from_variant (variant,
                                                     0  /* current time */,
                                                     &error);
      g_assert_error (error, RSD_VEHICLE_ERROR,
                      (gint) vectors[i].expected_error_code);
      g_assert_null (array);

      g_test_message ("Result: %s", error->message);
    }
}

/* Test that rsd_attribute_metadata_array_[to|from]_variant() works to round-trip
 * between variant and GPtrArray forms of RsdAttributeMetadata arrays. */
static void
test_utilities_attribute_metadata_array_valid (void)
{
  /* These must all be GVariants in text format, of type a(ss(uu)). */
  const gchar *vectors[] = {
    "[]",
    "[( 'testAttribute', [], (0, 0))]",
    "[( 'testAttribute', [], (0, 0)),"
     "( 'testAttribute2', [], (0, 0))]",
    "[( 'testAttributeAvailable', [{'type', <'attribute'>}," 
      "{'datatype', <'string'>}],  (1, 0))]",
    "[('testAttributeNotSupportedYet', [], (2, 0))]",
    "[('testAttributeNotSupportedSecurityPolicy', [{'unit', <'cm/s2'>}], (3, 0))]",
    "[('testAttributeNotSupportedBusinessPolicy', [], (4, 0))]",
    "[('testAttributeNotSupportedOther', [], (5, 0))]",
    "[('testAttributeReadable', [{'min', <0>}, {'max', <500>}], (0, 1))]",
    "[('testAttributeWritable', [], (0, 2))]",
    "[('testAttributeReadWrite',[], (0, 3))]",
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (GVariant) variant = NULL, variant2 = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GPtrArray/*<owned RsdAttributeMetadata>*/) array = NULL;

      /* Sanity check and parse the test data. */
      variant = g_variant_parse (G_VARIANT_TYPE ("a(sa{sv}(uu))"), vectors[i],
                                 NULL, NULL, &error);
      g_assert_no_error (error);

      /* Convert to an array. */
      array = rsd_attribute_metadata_array_from_variant (variant, &error);
      g_assert_no_error (error);
      g_assert_nonnull (array);

      /* Complete the round trip to the variant. */
      variant2 = rsd_attribute_metadata_array_to_variant (array);
      g_assert (g_variant_equal (variant, variant2));
    }
}

/* Test that rsd_attribute_metadata_array_from_variant() throws appropriate
 * errors when given an invalid variant. */
static void
test_utilities_attribute_metadata_array_from_variant_error (void)
{
  const struct {
    const gchar *variant;
    RsdVehicleError expected_error_code;
  } vectors[] = {
    { "'not an array'", RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "['invalid child']", RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "[('invalid attribute name', [{'unit', <'celsius'>}], (@u 0, @u 0))]", 
      RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE },
    { "[('testAttribute', @a{sv} [], (@u 6, @u 0))]",  // invalid availability 
      RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
    { "[('testAttribute', @a{sv} [], (@u 0, @u 5))]",    //invalid flags 
      RSD_VEHICLE_ERROR_INVALID_ATTRIBUTE_VALUE },
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (GVariant) variant = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GPtrArray/*<owned RsdAttributeMetadata>*/) array = NULL;

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expected error %u): %s", i,
                      vectors[i].expected_error_code, vectors[i].variant);

      /* Sanity check and parse the test data. */
      variant = g_variant_parse (NULL, vectors[i].variant, NULL, NULL, &error);
      g_assert_no_error (error);

      /* Try parsing. */
      array = rsd_attribute_metadata_array_from_variant (variant, &error);
      g_assert_error (error, RSD_VEHICLE_ERROR,
                      (gint) vectors[i].expected_error_code);
      g_assert_null (array);

      g_test_message ("Result: %s", error->message);
    }
}

/* Test that rsd_subscription_array_[to|from]_variant() works to round-trip
 * between variant and GPtrArray forms of RsdSubscription arrays. */
static void
test_utilities_subscription_array_valid (void)
{
  /* These must all be GVariants in text format, of type a(ssa{sv}). */
  const gchar *vectors[] = {
    "@a(sa{sv}) []",
    "[('*', @a{sv} {})]",
    "[('*', { "
      "'minimum-period': <@u 15>,"
      "'maximum-period': <@u 20>"
    "})]",
    "[('someAttribute', { "
      "'minimum-period': <@u 15>,"
      "'maximum-period': <@u 20>"
    "})]",
    "[('someAttribute', { "
      "'minimum-value': <@u 5>,"
      "'maximum-value': <@u 50>,"
      "'minimum-period': <@u 15>,"
      "'maximum-period': <@u 20>"
    "})]",
    "[( 'someAttribute', { "
      "'minimum-value': <@u 50>,"
      "'maximum-value': <@u 5>,"
      "'minimum-period': <@u 15>,"
      "'maximum-period': <@u 20>"
    "})]",
    "[('someAttribute', { "
      "'minimum-value': <@u 5>,"
      "'maximum-value': <@u 50>,"
      "'maximum-period': <@u 0>"
    "})]",
    "[('someAttribute', { "
      "'minimum-value': <@u 5>,"
      "'maximum-value': <@u 50>,"
      "'hysteresis': <@u 1>"
    "})]",
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (GVariant) variant = NULL, variant2 = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GPtrArray/*<owned RsdSubscription>*/) array = NULL;

      g_test_message ("Vector %" G_GSIZE_FORMAT ": %s", i, vectors[i]);

      /* Sanity check and parse the test data. */
      variant = g_variant_parse (G_VARIANT_TYPE ("a(sa{sv})"), vectors[i],
                                 NULL, NULL, &error);
      g_assert_no_error (error);

      /* Convert to an array. */
      array = rsd_subscription_array_from_variant (variant, &error);
      g_assert_no_error (error);
      g_assert_nonnull (array);

      /* Complete the round trip to the variant. */
      variant2 = rsd_subscription_array_to_variant (array);
      g_assert (g_variant_equal (variant, variant2));
    }
}

/* Test that rsd_subscription_array_from_variant() works to parse
 * #RsdSubscriptions from a valid variant. */
static void
test_utilities_subscription_array_from_variant_valid (void)
{
  const RsdSubscription subscription_string = {
     "someAttribute", NULL, NULL, NULL, 15, 20
  };
  const RsdSubscription subscription_wildcard = {
     "*", NULL, NULL, NULL, 0, G_MAXUINT
  };
  const RsdSubscription subscription_int = {
    "someAttribute", g_variant_new_uint32 (5),
    g_variant_new_uint32 (50), NULL, 15, 20
  };
  const RsdSubscription subscription_int_inverted = {
    "someAttribute", g_variant_new_uint32 (50),
    g_variant_new_uint32 (5), NULL, 15, 20
  };
  const RsdSubscription subscription_int_equal = {
    "someAttribute", g_variant_new_uint32 (5),
    g_variant_new_uint32 (5), NULL, 15, 20
  };
  const RsdSubscription subscription_int_hysteresis = {
    "someAttribute", g_variant_new_uint32 (20),
    g_variant_new_uint32 (60), g_variant_new_uint32 (5), 15, 20
  };
  const RsdSubscription subscription_no_period = {
    "someAttribute", g_variant_new_uint32 (5),
    g_variant_new_uint32 (50), NULL, 0, 0
  };
  const RsdSubscription subscription_default = {
    "someAttribute", NULL, NULL, NULL, 0, G_MAXUINT
  };

  /* These must all be GVariants in text format, of type a(ssa{sv}). */
  const struct {
    const gchar *variant;
    const RsdSubscription *subscriptions;
    gsize n_subscriptions;
  } vectors[] = {
    { "@a(sa{sv}) []", NULL, 0 },
    { "[('*', @a{sv} {})]", &subscription_wildcard, 1 },
    { "[('someAttribute', { "
        "'minimum-period': <@u 15>,"
        "'maximum-period': <@u 20>"
      "})]", &subscription_string, 1 },
    { "[('someAttribute', { "
        "'minimum-value': <@u 5>,"
        "'maximum-value': <@u 50>,"
        "'minimum-period': <@u 15>,"
        "'maximum-period': <@u 20>"
      "})]", &subscription_int, 1 },
    { "[('someAttribute', { "
        "'minimum-value': <@u 50>,"
        "'maximum-value': <@u 5>,"
        "'minimum-period': <@u 15>,"
        "'maximum-period': <@u 20>"
      "})]", &subscription_int_inverted, 1 },
    { "[('someAttribute', { "
        "'minimum-value': <@u 5>,"
        "'maximum-value': <@u 5>,"
        "'minimum-period': <@u 15>,"
        "'maximum-period': <@u 20>"
      "})]", &subscription_int_equal, 1 },
    { "[('someAttribute', { "
        "'minimum-value': <@u 20>,"
        "'maximum-value': <@u 60>,"
        "'hysteresis': <@u 5>,"
        "'minimum-period': <@u 15>,"
        "'maximum-period': <@u 20>"
      "})]", &subscription_int_hysteresis, 1 },
    { "[('someAttribute', { "
        "'minimum-value': <@u 5>,"
        "'maximum-value': <@u 50>,"
        "'minimum-period': <@u 0>,"
        "'maximum-period': <@u 0>"
      "})]", &subscription_no_period, 1 },
    { "[('someAttribute', @a{sv} {})]", &subscription_default, 1 },
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (GVariant) variant = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions = NULL;
      gsize j;

      g_test_message ("Vector %" G_GSIZE_FORMAT ": %s", i, vectors[i].variant);

      /* Sanity check and parse the test data. */
      variant = g_variant_parse (G_VARIANT_TYPE ("a(sa{sv})"),
                                 vectors[i].variant, NULL, NULL, &error);
      g_assert_no_error (error);

      /* Convert to an info. */
      subscriptions = rsd_subscription_array_from_variant (variant, &error);
      g_assert_no_error (error);
      g_assert_nonnull (subscriptions);

      for (j = 0; j < subscriptions->len; j++)
        {
          const RsdSubscription *subscription = subscriptions->pdata[j];

          g_assert_cmpstr (subscription->attribute_name, ==,
                           vectors[i].subscriptions[j].attribute_name);

          if (vectors[i].subscriptions[j].minimum_value != NULL)
            g_assert_true (g_variant_equal (subscription->minimum_value,
                                            vectors[i].subscriptions[j].minimum_value));
          if (vectors[i].subscriptions[j].maximum_value != NULL)
            g_assert_true (g_variant_equal (subscription->maximum_value,
                                            vectors[i].subscriptions[j].maximum_value));

          g_assert_cmpuint (subscription->minimum_period, ==,
                            vectors[i].subscriptions[j].minimum_period);
          g_assert_cmpuint (subscription->maximum_period, ==,
                            vectors[i].subscriptions[j].maximum_period);
        }

      g_assert_cmpuint (subscriptions->len, ==, vectors[i].n_subscriptions);
    }

  /* Clean up. */
  g_variant_unref (subscription_int.minimum_value);
  g_variant_unref (subscription_int.maximum_value);
  g_variant_unref (subscription_int_inverted.minimum_value);
  g_variant_unref (subscription_int_inverted.maximum_value);
  g_variant_unref (subscription_int_equal.minimum_value);
  g_variant_unref (subscription_int_equal.maximum_value);
  g_variant_unref (subscription_int_hysteresis.minimum_value);
  g_variant_unref (subscription_int_hysteresis.maximum_value);
  g_variant_unref (subscription_int_hysteresis.hysteresis);
  g_variant_unref (subscription_no_period.minimum_value);
  g_variant_unref (subscription_no_period.maximum_value);
}

/* Test that rsd_subscription_array_from_variant() throws appropriate errors
 * when given an invalid variant. */
static void
test_utilities_subscription_array_from_variant_error (void)
{
  /* These must all be GVariants in text format, of type as. */
  const struct {
    const gchar *vector;
    RsdVehicleError expected_error_code;
  } vectors[] = {
    { "'not a list'", RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION },
    { "@as []", RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION },
    { "[('not an attribute', { "
        "'minimum-period': <@u 15>,"
        "'maximum-period': <@u 20>"
      "})]", RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE },
    /* Wildcard with wrong zone path: */
    { "[('', { "
        "'minimum-period': <@u 15>,"
        "'maximum-period': <@u 20>"
      "})]", RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE },
    /* Different value types: */
    { "[('someAttribute', { "
        "'minimum-value': <@x 50>,"
        "'maximum-value': <@u 5>,"
        "'minimum-period': <@u 15>,"
        "'maximum-period': <@u 20>"
      "})]", RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION },
    /* Different value types: */
    { "[('someAttribute', { "
        "'minimum-value': <@u 5>,"
        "'maximum-value': <@u 50>,"
        "'hysteresis': <@x 5>,"
        "'minimum-period': <@u 15>,"
        "'maximum-period': <@u 20>"
      "})]", RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION },
    /* Same-typed non-numeric values: */
    { "[('someAttribute', { "
        "'minimum-value': <'value'>,"
        "'maximum-value': <'value'>,"
        "'minimum-period': <@u 15>,"
        "'maximum-period': <@u 20>"
      "})]", RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION },
    { "[('someAttribute', { "
        "'minimum-period': <@u 25>,"
        "'maximum-period': <@u 20>"
      "})]", RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION },
    /* Incorrect attribute types: */
    { "[('someAttribute', { "
        "'minimum-period': <@x 25>,"
        "'maximum-period': <@x 20>"
      "})]", RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION },
    /* Unknown attribute: */
    { "[('someAttribute', { "
        "'this is not a key': <'value'>"
      "})]", RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION },
    /* Hysteresis without a value range: */
    { "[('someAttribute', { "
        "'hysteresis': <@u 5>"
      "})]", RSD_VEHICLE_ERROR_INVALID_SUBSCRIPTION },
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (GVariant) variant = NULL;
      g_autoptr (GError) error = NULL;
      g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions = NULL;

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expected error %u): %s",
                      i, vectors[i].expected_error_code, vectors[i].vector);

      /* Sanity check and parse the test data. */
      variant = g_variant_parse (NULL, vectors[i].vector, NULL, NULL, &error);
      g_assert_no_error (error);

      /* Try parsing. */
      subscriptions = rsd_subscription_array_from_variant (variant, &error);
      g_assert_error (error, RSD_VEHICLE_ERROR,
                      (gint) vectors[i].expected_error_code);
      g_assert_null (subscriptions);

      g_test_message ("Result: %s", error->message);
    }
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/utilities/attribute-info-array/valid",
                   test_utilities_attribute_info_array_valid);
  g_test_add_func ("/utilities/attribute-info-array/from-variant/error",
                   test_utilities_attribute_info_array_from_variant_error);

  g_test_add_func ("/utilities/attribute-metadata-array/valid",
                   test_utilities_attribute_metadata_array_valid);
  g_test_add_func ("/utilities/attribute-metadata-array/from-variant/error",
                   test_utilities_attribute_metadata_array_from_variant_error);

  g_test_add_func ("/utilities/subscription-array/valid",
                   test_utilities_subscription_array_valid);
  g_test_add_func ("/utilities/subscription-array-from-variant/valid",
                   test_utilities_subscription_array_from_variant_valid);
  g_test_add_func ("/utilities/subscription-array-from-variant/error",
                   test_utilities_subscription_array_from_variant_error);

  return g_test_run ();
}
