/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gio/gio.h>
#include <glib.h>
#include <locale.h>
#include <string.h>
#include <sys/socket.h>

#include "dbus-utilities.h"

/**
 * SECTION:dbus-utilities
 * @short_description: DBus utilities
 *
 * Provide utility functions to manage DBus.
 *
 * #dbus-utilities is available since 0.1.0
 */

static void connection_cb (GObject      *obj,
                           GAsyncResult *result,
                           gpointer      user_data);

static void
queue_free (gpointer data)
{
  GQueue *queue = data;

  g_queue_free_full (queue, g_free);
}

/**
 * dbus_fixture_setup:
 * @fixture: a dbus fixture
 * @test_data: pointer to the test data
 *
 * Set up a #DBusFixture. Designed to be used with g_test_add().
 *
 * Since: 0.2.0
 */
void
dbus_fixture_setup (DBusFixture   *fixture,
                    gconstpointer  test_data)
{
  int pair[2];
  g_autoptr (GError) error = NULL;
  g_autoptr (GSocket) server_socket = NULL, client_socket = NULL;
  g_autoptr (GSocketConnection) server_stream = NULL, client_stream = NULL;
  g_autofree gchar *guid = NULL;

  /* Set up the table to store expected event queues so we can check on teardown
   * that they’re all empty. */
  fixture->queues = g_hash_table_new_full (g_direct_hash, g_direct_equal, NULL,
                                           queue_free);

  /* Create a socket pair to communicate over. */
  g_assert_cmpint (socketpair (AF_UNIX, SOCK_STREAM, 0, pair), ==, 0);

  /* Set up the server side of things. */
  server_socket = g_socket_new_from_fd (pair[1], &error);
  g_assert_no_error (error);

  server_stream = g_socket_connection_factory_create_connection (server_socket);
  g_assert_nonnull (server_stream);

  guid = g_dbus_generate_guid ();
  g_dbus_connection_new (G_IO_STREAM (server_stream), guid,
                         G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_SERVER |
                         G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_ALLOW_ANONYMOUS,
                         NULL, NULL,
                         connection_cb, &fixture->server_connection);

  /* Set up the client side of things. */
  client_socket = g_socket_new_from_fd (pair[0], &error);
  g_assert_no_error (error);

  client_stream = g_socket_connection_factory_create_connection (client_socket);
  g_assert_nonnull (client_stream);

  g_dbus_connection_new (G_IO_STREAM (client_stream), NULL,
                         G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT |
                         G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_ALLOW_ANONYMOUS,
                         NULL, NULL,
                         connection_cb, &fixture->client_connection);

  /* Wait until both are ready. */
  while (fixture->server_connection == NULL ||
         fixture->client_connection == NULL)
    g_main_context_iteration (NULL, TRUE);
}

static void
connection_cb (GObject      *obj,
               GAsyncResult *result,
               gpointer      user_data)
{
  GDBusConnection **connection = user_data;
  g_autoptr (GError) error = NULL;

  *connection = g_dbus_connection_new_finish (result, &error);
  g_assert_no_error (error);
}

/**
 * dbus_fixture_teardown:
 * @fixture: a dbus fixture
 * @test_data: pointer to the test data
 *
 * Tear down a #DBusFixture. Designed to be used with g_test_add().
 *
 * Since: 0.2.0
 */

void
dbus_fixture_teardown (DBusFixture   *fixture,
                       gconstpointer  test_data)
{
  GHashTableIter iter;
  GQueue/*<owned ExpectedEvent>*/ *queue;

  g_clear_object (&fixture->client_connection);
  g_clear_object (&fixture->server_connection);

  /* Check all the expected event queues are empty. */
  g_hash_table_iter_init (&iter, fixture->queues);

  while (g_hash_table_iter_next (&iter, NULL, (gpointer *) &queue))
    {
      GList *l;

      for (l = queue->head; l != NULL; l = l->next)
        {
          const ExpectedEvent *event;

          event = l->data;
          g_test_message ("Expected event %u", event->type);
        }

      g_assert_true (g_queue_is_empty (queue));
    }

  g_clear_pointer (&fixture->queues, g_hash_table_unref);
}

/**
 * async_result_data_new:
 *
 * Create a new #AsyncResultData structure.
 *
 * Returns: (transfer full): a new #AsyncResultData
 * Since: 0.2.0
 */
AsyncResultData *
async_result_data_new (void)
{
  return g_new0 (AsyncResultData, 1);
}

/**
 * async_result_data_free:
 * @data: (transfer full): an #AsyncResultData
 *
 * Free an #AsyncResultData.
 *
 * Since: 0.2.0
 */
void
async_result_data_free (AsyncResultData *data)
{
  g_clear_object (&data->obj);
  g_clear_object (&data->result);
  g_free (data);
}

/**
 * async_result_data_callback:
 * @obj: as specified by #GAsyncResult
 * @result: as specified by #GAsyncResult
 * @user_data: as specified by #GAsyncResult
 *
 * Callback function to pass as the #GAsyncReadyCallback to an asynchronous
 * function to populate the #AsyncResultData. Pass the #AsyncResultData as the
 * accompanying user data.
 *
 * Since: 0.2.0
 */
void
async_result_data_callback (GObject      *obj,
                            GAsyncResult *result,
                            gpointer      user_data)
{
  AsyncResultData *data = user_data;

  data->obj = g_object_ref (obj);
  data->result = g_object_ref (result);
}

/**
 * async_result_data_yield:
 * @data: an #AsyncResultData
 * @context: (nullable): the #GMainContext to iterate, or %NULL to use the
 *    global default main context
 *
 * Block until the given #AsyncResultData’s callback has been invoked; i.e. the
 * asynchronous operation it’s being used in, completes. Iterate the given
 * @context until then.
 *
 * Since: 0.2.0
 */
void
async_result_data_yield (AsyncResultData *data,
                         GMainContext    *context)
{
  while (data->result == NULL || data->obj == NULL)
    g_main_context_iteration (context, TRUE);
}

/* Server-side handler for an incoming D-Bus method call. */
static void
method_call_cb (GDBusConnection       *connection,
                const gchar           *sender,
                const gchar           *object_path,
                const gchar           *interface_name,
                const gchar           *method_name,
                GVariant              *parameters,
                GDBusMethodInvocation *invocation,
                gpointer               user_data)
{
  g_autofree ExpectedEvent *next_event = NULL;
  const ExpectedMethodCall *next_call;
  GQueue *event_queue = user_data;
  g_autoptr (GVariant) next_call_parameters = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *parameters_string = NULL;

  parameters_string = g_variant_print (parameters, TRUE);
  g_test_message ("Received %s → %s, %s.%s(%s)", sender, object_path,
                  interface_name, method_name, parameters_string);

  next_event = g_queue_pop_head (event_queue);
  g_assert_nonnull (next_event);
  g_assert_cmpuint (next_event->type, ==, EVENT_METHOD_CALL);

  next_call = &next_event->method_call;

  next_call_parameters = g_variant_parse (NULL, next_call->parameters,
                                          NULL, NULL, &error);
  g_assert_no_error (error);

  g_test_message ("Expected %s → %s, %s.%s(%s)", next_call->sender,
                  next_call->object_path, next_call->interface_name,
                  next_call->method_name, next_call->parameters);

  /* Check the header. */
  g_assert_cmpstr (sender, ==, next_call->sender);
  g_assert_cmpstr (object_path, ==, next_call->object_path);
  g_assert_cmpstr (interface_name, ==, next_call->interface_name);
  g_assert_cmpstr (method_name, ==, next_call->method_name);

  /* Check the parameters. */
  g_assert_true (g_variant_equal (parameters, next_call_parameters));

  /* Return a value or an error. */
  g_assert ((next_call->return_value == NULL) !=
            (next_call->return_error.message == NULL));

  if (next_call->return_value != NULL)
    {
      g_autoptr (GVariant) next_call_return_value = NULL;

      g_test_message ("Returning %s", next_call->return_value);

      next_call_return_value = g_variant_parse (NULL, next_call->return_value,
                                                NULL, NULL, &error);
      g_assert_no_error (error);

      g_dbus_method_invocation_return_value (invocation,
                                             next_call_return_value);
    }
  else
    {
      g_test_message ("Returning error (%s, %u): %s",
                      g_quark_to_string (next_call->return_error.domain),
                      next_call->return_error.code,
                      next_call->return_error.message);

      g_dbus_method_invocation_return_error_literal (invocation,
                                                     next_call->return_error.domain,
                                                     next_call->return_error.code,
                                                     next_call->return_error.message);
    }
}

/* Server-side handler for an incoming D-Bus `GetProperty` call. */
static GVariant *
get_property_cb (GDBusConnection  *connection,
                 const gchar      *sender,
                 const gchar      *object_path,
                 const gchar      *interface_name,
                 const gchar      *property_name,
                 GError          **error,
                 gpointer          user_data)
{
  g_autofree ExpectedEvent *next_event = NULL;
  const ExpectedGetProperty *next_prop;
  GQueue *event_queue = user_data;
  g_autoptr (GError) child_error = NULL;

  g_test_message ("Received get for %s → %s, %s.%s", sender, object_path,
                  interface_name, property_name);

  next_event = g_queue_pop_head (event_queue);
  g_assert_nonnull (next_event);
  g_assert_cmpuint (next_event->type, ==, EVENT_GET_PROPERTY);

  next_prop = &next_event->get_property;

  g_test_message ("Expected get for %s → %s, %s.%s", next_prop->sender,
                  next_prop->object_path, next_prop->interface_name,
                  next_prop->property_name);

  /* Check the header. */
  g_assert_cmpstr (sender, ==, next_prop->sender);
  g_assert_cmpstr (object_path, ==, next_prop->object_path);
  g_assert_cmpstr (interface_name, ==, next_prop->interface_name);
  g_assert_cmpstr (property_name, ==, next_prop->property_name);

  /* Return a value or an error. */
  g_assert ((next_prop->return_value == NULL) !=
            (next_prop->return_error.message == NULL));

  if (next_prop->return_value != NULL)
    {
      g_autoptr (GVariant) next_call_return_value = NULL;

      g_test_message ("Returning %s", next_prop->return_value);

      next_call_return_value = g_variant_parse (NULL, next_prop->return_value,
                                                NULL, NULL, &child_error);
      g_assert_no_error (child_error);

      return g_steal_pointer (&next_call_return_value);
    }
  else
    {
      g_test_message ("Returning error (%s, %u): %s",
                      g_quark_to_string (next_prop->return_error.domain),
                      next_prop->return_error.code,
                      next_prop->return_error.message);

      g_set_error_literal (error, next_prop->return_error.domain,
                           next_prop->return_error.code,
                           next_prop->return_error.message);
      return NULL;
    }
}

/* Server-side handler for an incoming D-Bus `SetProperty` call. */
static gboolean
set_property_cb (GDBusConnection  *connection,
                 const gchar      *sender,
                 const gchar      *object_path,
                 const gchar      *interface_name,
                 const gchar      *property_name,
                 GVariant         *value,
                 GError          **error,
                 gpointer          user_data)
{
  g_autofree ExpectedEvent *next_event = NULL;
  const ExpectedSetProperty *next_prop;
  GQueue *event_queue = user_data;
  g_autofree gchar *value_string = NULL;

  value_string = g_variant_print (value, TRUE);
  g_test_message ("Received set for %s → %s, %s.%s = %s", sender, object_path,
                  interface_name, property_name, value_string);

  next_event = g_queue_pop_head (event_queue);
  g_assert_nonnull (next_event);
  g_assert_cmpuint (next_event->type, ==, EVENT_SET_PROPERTY);

  next_prop = &next_event->set_property;

  g_test_message ("Expected set for %s → %s, %s.%s", next_prop->sender,
                  next_prop->object_path, next_prop->interface_name,
                  next_prop->property_name);

  /* Check the header. */
  g_assert_cmpstr (sender, ==, next_prop->sender);
  g_assert_cmpstr (object_path, ==, next_prop->object_path);
  g_assert_cmpstr (interface_name, ==, next_prop->interface_name);
  g_assert_cmpstr (property_name, ==, next_prop->property_name);

  /* Return success or an error. */
  if (next_prop->return_error.message == NULL)
    {
      g_test_message ("Returning success");

      return TRUE;
    }
  else
    {
      g_test_message ("Returning error (%s, %u): %s",
                      g_quark_to_string (next_prop->return_error.domain),
                      next_prop->return_error.code,
                      next_prop->return_error.message);

      g_set_error_literal (error, next_prop->return_error.domain,
                           next_prop->return_error.code,
                           next_prop->return_error.message);
      return FALSE;
    }
}

static const GDBusInterfaceVTable object_vtable = {
  method_call_cb,
  get_property_cb,
  set_property_cb,
};

/* Copy @events into an owned #GQueue of events. */
static GQueue/*<owned ExpectedEvent>*/ *
queue_new_from_expected_event_array (const ExpectedEvent *events,
                                     gsize                n_events)
{
  g_autoptr (GQueue/*<unowned ExpectedEvent>*/) queue = NULL;
  gsize i;

  queue = g_queue_new ();

  for (i = 0; i < n_events; i++)
    {
      const ExpectedEvent *event = &events[i];
      g_autofree ExpectedEvent *event_copy = NULL;

      event = &events[i];
      event_copy = g_new0 (ExpectedEvent, 1);
      memcpy (event_copy, event, sizeof (*event));

      g_queue_push_tail (queue, g_steal_pointer (&event_copy));
    }

  return g_steal_pointer (&queue);
}

/**
 * dbus_fixture_register_object:
 * @fixture: a #DBusFixture
 * @object_path: path to export the object at
 * @interface_info: introspection data for the interface
 * @events: (array length=n_events): potentially empty array of expected D-Bus
 *    events to be seen on this object
 * @n_events: number of events in @events
 * @error: return location for a #GError, or %NULL
 *
 * Register a D-Bus object on the test fixture’s server connection, so it is
 * exposed to the client code under test. As well as registering the object on
 * the bus, this adds its queue of expected @events to the test harness, to be
 * checked against the actual events seen on the bus.
 *
 * Returns: registration ID of the object, for use with
 *    dbus_fixture_unregister_object()
 * Since: 0.2.0
 */
guint
dbus_fixture_register_object (DBusFixture               *fixture,
                              const gchar               *object_path,
                              const GDBusInterfaceInfo  *interface_info,
                              const ExpectedEvent       *events,
                              gsize                      n_events,
                              GError                   **error)
{
  g_autoptr (GQueue/*<unowned ExpectedEvent>*/) queue = NULL;
  guint id;

  /* Create the expected event queue and store it to be checked on teardown. */
  queue = queue_new_from_expected_event_array (events, n_events);

  /* Create the object on the server side. */
  id = g_dbus_connection_register_object (fixture->server_connection,
                                          object_path,
                                          (GDBusInterfaceInfo *) interface_info,
                                          &object_vtable,
                                          queue, NULL, error);

  g_hash_table_insert (fixture->queues, GINT_TO_POINTER (id),
                       g_steal_pointer (&queue));

  return id;
}

/**
 * dbus_fixture_unregister_object:
 * @fixture: a #DBusFixture
 * @id: registration ID of the object to unregister
 *
 * Unregister an object registered with dbus_fixture_register_object().
 *
 * Since: 0.2.0
 */
void
dbus_fixture_unregister_object (DBusFixture *fixture,
                                guint        id)
{
  GQueue/*<unowned ExpectedEvent>*/ *queue;
  GList *l;

  /* Check the event queue is empty. */
  queue = g_hash_table_lookup (fixture->queues, GINT_TO_POINTER (id));
  g_assert (queue != NULL);

  for (l = queue->head; l != NULL; l = l->next)
    {
      const ExpectedEvent *event;

      event = l->data;
      g_test_message ("Expected event %u", event->type);
    }

  g_assert_true (g_queue_is_empty (queue));

  g_hash_table_remove (fixture->queues, GINT_TO_POINTER (id));

  g_dbus_connection_unregister_object (fixture->server_connection, id);
}
