/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2020 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <string.h>
#include "librhosydd/types.h"

/**
 * SECTION: types
 * @short_description: Types declarations
 *
 * Provides types conversions.
 *
 * #types is available since 0.1.0
 */

/**
 * rsd_variant_type_from_vss_type
 * @value_type: data type of value in string format.
 *
 * Gives the GVariantType equivalent of the
 * 'value_type' which is in string format;
 *
 * Returns: GVariantType
 *
 * Since: 0.2.0
 */
const GVariantType *
rsd_variant_type_from_vss_type (const gchar *value_type)
{
    if(!value_type || !strlen(value_type))
         return NULL;
    if(!strcmp(value_type, "Uint8") || !strcmp(value_type, "Int8"))
       return G_VARIANT_TYPE_BYTE;
    if(!strcmp(value_type, "UInt16"))
       return G_VARIANT_TYPE_UINT16;
    if(!strcmp(value_type, "Int16"))
       return G_VARIANT_TYPE_INT16;
    if(!strcmp(value_type, "Int32"))
       return G_VARIANT_TYPE_INT32;
    if(!strcmp(value_type, "UInt32"))
       return G_VARIANT_TYPE_UINT32;
    if(!strcmp(value_type, "Int64"))
       return G_VARIANT_TYPE_INT64;
    if(!strcmp(value_type, "UInt64"))
       return G_VARIANT_TYPE_UINT64;
    if(!strcmp(value_type, "Boolean"))
       return G_VARIANT_TYPE_BOOLEAN;
    if(!strcmp(value_type, "Float") || !strcmp(value_type, "Double"))
       return G_VARIANT_TYPE_DOUBLE;
    if(!strcmp(value_type, "String"))
       return G_VARIANT_TYPE_STRING;    
    return NULL;
}

