# DIST_SUBDIRS needs to be specified with the root directory last so that
# maintainer-clean does not delete build-aux/* before recursing, since it is
# needed for any automake invocation.
SUBDIRS = . daemon/tests librhosydd/tests libcroesor/tests librhosydd/docs libcroesor/docs tests po
DIST_SUBDIRS = daemon/tests librhosydd/tests libcroesor/tests librhosydd/docs libcroesor/docs tests po

ACLOCAL_AMFLAGS = -I m4

AM_DISTCHECK_CONFIGURE_FLAGS =
CLEANFILES = $(BUILT_SOURCES)
DISTCLEANFILES =
MAINTAINERCLEANFILES =
EXTRA_DIST =
bin_PROGRAMS =
bin_SCRIPTS =
backendbin_PROGRAMS =
man8_MANS =
VAPIGEN_VAPIS =
lib_LTLIBRARIES =
noinst_LTLIBRARIES =
BUILT_SOURCES =
GITIGNOREFILES =
pkgconfig_DATA =
noinst_PROGRAMS =

# Vehicle device daemon internal library
noinst_LTLIBRARIES += daemon/libdaemon.la

daemon_libdaemon_la_SOURCES = \
	daemon/aggregate-vehicle.c \
	daemon/aggregate-vehicle.h \
	daemon/peer-manager.c \
	daemon/peer-manager.h \
	daemon/service.c \
	daemon/service.h \
	daemon/vehicle-multiplexer.c \
	daemon/vehicle-multiplexer.h \
	$(NULL)

daemon_libdaemon_la_CPPFLAGS = \
	-I$(top_srcdir) \
	-I$(top_builddir) \
	-DG_LOG_DOMAIN=\"rhosydd\" \
	$(AM_CPPFLAGS) \
	$(NULL)

daemon_libdaemon_la_CFLAGS = \
	$(DAEMON_CFLAGS) \
	$(CODE_COVERAGE_CFLAGS) \
	$(WARN_CFLAGS) \
	$(AM_CFLAGS) \
	$(NULL)

daemon_libdaemon_la_LIBADD = \
	$(top_builddir)/librhosydd/librhosydd-@RSD_API_VERSION@.la \
	$(top_builddir)/libcroesor/libcroesor-@CSR_API_VERSION@.la \
	$(top_builddir)/libinternal/libinternal.la \
	$(DAEMON_LIBS) \
	$(CODE_COVERAGE_LDFLAGS) \
	$(AM_LIBADD) \
	$(NULL)

daemon_libdaemon_la_LDFLAGS = \
	-no-undefined \
	$(WARN_LDFLAGS) \
	$(AM_LDFLAGS) \
	$(NULL)

# Vehicle device daemon
bin_PROGRAMS += daemon/rhosydd

daemon_rhosydd_SOURCES = \
	daemon/main.c \
	$(NULL)

daemon_rhosydd_CPPFLAGS = \
	-I$(top_srcdir) \
	-I$(top_builddir) \
	-DG_LOG_DOMAIN=\"rhosydd\" \
	-DLOCALEDIR=\""$(localedir)"\" \
	$(AM_CPPFLAGS) \
	$(NULL)

daemon_rhosydd_CFLAGS = \
	$(DAEMON_CFLAGS) \
	$(CODE_COVERAGE_CFLAGS) \
	$(WARN_CFLAGS) \
	$(AM_CFLAGS) \
	$(NULL)

daemon_rhosydd_LDADD = \
	$(top_builddir)/daemon/libdaemon.la \
	$(top_builddir)/libcroesor/libcroesor-@CSR_API_VERSION@.la \
	$(DAEMON_LIBS) \
	$(CODE_COVERAGE_LDFLAGS) \
	$(AM_LIBADD) \
	$(NULL)

daemon_rhosydd_LDFLAGS = \
	-no-undefined \
	$(WARN_LDFLAGS) \
	$(AM_LDFLAGS) \
	$(NULL)

# systemd and D-Bus service files.
%.service: %.service.in Makefile
	$(AM_V_GEN) sed \
		-e "s|[@]bindir[@]|$(bindir)|" \
		-e "s|[@]backendbindir[@]|$(backendbindir)|" \
		-e "s|[@]DAEMON_USER[@]|$(DAEMON_USER)|" \
		-e "s|[@]BACKEND_USER[@]|$(BACKEND_USER)|" \
		$< > $@.tmp && mv $@.tmp $@

%.conf: %.conf.in Makefile
	$(AM_V_GEN) sed \
		-e "s|[@]DAEMON_USER[@]|$(DAEMON_USER)|" \
		-e "s|[@]BACKEND_USER[@]|$(BACKEND_USER)|" \
		$< > $@.tmp && mv $@.tmp $@

systemdsystemunit_DATA = \
	backends/mock/rhosydd-mock-backend.service \
	backends/speedo/rhosydd-speedo-backend.service \
	daemon/rhosydd.service \
	$(NULL)

dbusconfdir = $(sysconfdir)/dbus-1/system.d
dbusconf_DATA = \
	backends/mock/org.apertis.Rhosydd1.Backends.Mock.conf \
	backends/speedo/org.apertis.Rhosydd1.Backends.Speedo.conf \
	daemon/org.apertis.Rhosydd1.conf \
	$(NULL)

dbus_servicedir = $(datadir)/dbus-1/system-services
dbus_service_DATA = \
	backends/mock/org.apertis.Rhosydd1.Backends.Mock.service \
	backends/speedo/org.apertis.Rhosydd1.Backends.Speedo.service \
	daemon/org.apertis.Rhosydd1.service \
	$(NULL)

EXTRA_DIST += \
	$(dbus_service_DATA:%=%.in) \
	$(systemdsystemunit_DATA:%=%.in) \
	$(dbusconf_DATA:%=%.in) \
	$(NULL)

CLEANFILES += \
	$(dbus_service_DATA) \
	$(systemdsystemunit_DATA) \
	$(dbusconf_DATA) \
	$(NULL)

# sysusers.d files
systemdsysusers_DATA = \
	backends/rhosydd-backends.conf \
	daemon/rhosydd-daemon.conf \
	$(NULL)

EXTRA_DIST += $(systemdsysusers_DATA:%=%.in)
CLEANFILES += $(systemdsysusers_DATA)

# polkit policy and example (non-installed) rules file **for development use
# only**.
%.policy: %.policy.in Makefile
	$(AM_V_GEN) sed \
		-e "s|[@]DAEMON_USER[@]|$(DAEMON_USER)|" \
		-e "s|[@]BACKEND_USER[@]|$(BACKEND_USER)|" \
		$< > $@.tmp && mv $@.tmp $@

%.rules: %.rules.in Makefile
	$(AM_V_GEN) sed \
		-e "s|[@]DAEMON_USER[@]|$(DAEMON_USER)|" \
		-e "s|[@]BACKEND_USER[@]|$(BACKEND_USER)|" \
		$< > $@.tmp && mv $@.tmp $@

polkit_policydir = $(datadir)/polkit-1/actions
polkit_policy_DATA = libcroesor/org.apertis.Rhosydd1.policy
polkit_rulesdir = $(datadir)/polkit-1/rules.d
polkit_rules_DATA = daemon/30-rhosydd.rules
EXTRA_DIST += \
	$(polkit_policy_DATA:%=%.in) \
	$(polkit_rules_DATA:%=%.in) \
	daemon/30-rhosydd-insecure.rules \
	$(NULL)
CLEANFILES += \
	$(polkit_policy_DATA) \
	$(polkit_rules_DATA)
	$(NULL)

# AppArmor policy
$(apparmor_policy_DATA): %: %.in Makefile
	$(AM_V_GEN) sed \
		-e "s|[@]backendbindir[@]|$(backendbindir)|" \
		$< > $@.tmp && mv $@.tmp $@

apparmor_policydir = $(sysconfdir)/apparmor.d
apparmor_policy_DATA = \
	backends/mock/usr.lib.rhosydd.rhosydd-mock-backend \
	backends/speedo/usr.lib.rhosydd.rhosydd-speedo-backend \
	$(NULL)
dist_apparmor_policy_DATA = \
	clients/usr.bin.rhosydd-client \
	daemon/usr.bin.rhosydd \
	$(NULL)
EXTRA_DIST += $(apparmor_policy_DATA:%=%.in)
CLEANFILES += $(apparmor_policy_DATA)

# Vehicle device daemon mock backend
backendbindir = $(libexecdir)/rhosydd-@RSD_API_VERSION@
mockbackenddir = $(libexecdir)/rhosydd-@RSD_API_VERSION@
mockbackend_DATA = backends/mock/vehicle_attributes_spec.yaml
EXTRA_DIST += $(mockbackend_DATA)

backendbin_PROGRAMS += backends/mock/rhosydd-mock-backend

backends_mock_rhosydd_mock_backend_SOURCES = \
	backends/mock/main.c \
	$(NULL)

backends_mock_rhosydd_mock_backend_CPPFLAGS = \
	-I$(top_srcdir) \
	-I$(top_builddir) \
	-DG_LOG_DOMAIN=\"rhosydd-mock-backend\" \
	-DLOCALEDIR=\""$(localedir)"\" \
        -DMOCKBACKENDDIR=\""$(mockbackenddir)"\" \
	$(AM_CPPFLAGS) \
	$(NULL)

backends_mock_rhosydd_mock_backend_CFLAGS = \
	$(BACKENDS_MOCK_CFLAGS) \
	$(CODE_COVERAGE_CFLAGS) \
	$(WARN_CFLAGS) \
	$(AM_CFLAGS) \
	$(NULL)

backends_mock_rhosydd_mock_backend_LDADD = \
	$(top_builddir)/librhosydd/librhosydd-@RSD_API_VERSION@.la \
	$(top_builddir)/libcroesor/libcroesor-@CSR_API_VERSION@.la \
	$(BACKENDS_MOCK_LIBS) \
	$(CODE_COVERAGE_LDFLAGS) \
	$(AM_LIBADD) \
	$(NULL)

backends_mock_rhosydd_mock_backend_LDFLAGS = \
	-no-undefined \
	$(WARN_LDFLAGS) \
	$(AM_LDFLAGS) \
	$(NULL)

# Vehicle device daemon speedo backend
speedobackenddir = $(libexecdir)/rhosydd-@RSD_API_VERSION@
speedobackend_DATA = backends/speedo/speedo_attributes.json
EXTRA_DIST += $(speedobackend_DATA)
#CLEANFILES += $(speedobackend_DATA)

backendbin_PROGRAMS += backends/speedo/rhosydd-speedo-backend

backends_speedo_rhosydd_speedo_backend_SOURCES = \
	backends/speedo/main.c \
	backends/speedo/vehicle.c \
	backends/speedo/vehicle.h \
        backends/speedo/vehicle_builder.h \
        backends/speedo/vehicle_builder.c \
	$(NULL)

backends_speedo_rhosydd_speedo_backend_CPPFLAGS = \
	-I$(top_srcdir) \
	-I$(top_builddir) \
	-DG_LOG_DOMAIN=\"rhosydd-speedo-backend\" \
	-DLOCALEDIR=\""$(localedir)"\" \
        -DSPEEDOBACKENDDIR=\""$(speedobackenddir)"\" \
	$(AM_CPPFLAGS) \
	$(NULL)

backends_speedo_rhosydd_speedo_backend_CFLAGS = \
	$(BACKENDS_SPEEDO_CFLAGS) \
	$(CODE_COVERAGE_CFLAGS) \
	$(WARN_CFLAGS) \
	$(AM_CFLAGS) \
	$(NULL)

backends_speedo_rhosydd_speedo_backend_LDADD = \
	$(top_builddir)/librhosydd/librhosydd-@RSD_API_VERSION@.la \
	$(top_builddir)/libcroesor/libcroesor-@CSR_API_VERSION@.la \
	$(top_builddir)/libinternal/libinternal.la \
	$(BACKENDS_SPEEDO_LIBS) \
	$(LIBM) \
	$(CODE_COVERAGE_LDFLAGS) \
	$(AM_LIBADD) \
	$(NULL)

backends_speedo_rhosydd_speedo_backend_LDFLAGS = \
	-no-undefined \
	$(WARN_LDFLAGS) \
	$(AM_LDFLAGS) \
	$(NULL)

# Internal utility library
noinst_LTLIBRARIES += libinternal/libinternal.la

libinternal_libinternal_la_SOURCES = \
	libinternal/arrays.c \
	libinternal/arrays.h \
	libinternal/logging.h \
	libinternal/timestamped-pointer.c \
	libinternal/timestamped-pointer.h \
	$(NULL)

libinternal_libinternal_la_CPPFLAGS = \
	-I$(top_srcdir) \
	-I$(top_builddir) \
	-DG_LOG_DOMAIN=\"libinternal\" \
	$(AM_CPPFLAGS) \
	$(NULL)

libinternal_libinternal_la_CFLAGS = \
	$(INTERNAL_CFLAGS) \
	$(CODE_COVERAGE_CFLAGS) \
	$(WARN_CFLAGS) \
	$(AM_CFLAGS) \
	$(NULL)

libinternal_libinternal_la_LIBADD = \
	$(INTERNAL_LIBS) \
	$(CODE_COVERAGE_LDFLAGS) \
	$(AM_LIBADD) \
	$(NULL)

libinternal_libinternal_la_LDFLAGS = \
	-no-undefined \
	$(WARN_LDFLAGS) \
	$(AM_LDFLAGS) \
	$(NULL)

# Command line client
bin_PROGRAMS += clients/rhosydd-client

clients_rhosydd_client_SOURCES = \
	clients/rhosydd-client.c \
	$(NULL)

clients_rhosydd_client_CPPFLAGS = \
	-I$(top_srcdir) \
	-I$(top_builddir) \
	-DG_LOG_DOMAIN=\"rhosydd-client\" \
	-DLOCALEDIR=\""$(localedir)"\" \
	$(AM_CPPFLAGS) \
	$(NULL)

clients_rhosydd_client_CFLAGS = \
	$(CLIENTS_CFLAGS) \
	$(CODE_COVERAGE_CFLAGS) \
	$(WARN_CFLAGS) \
	$(AM_CFLAGS) \
	$(NULL)

clients_rhosydd_client_LDADD = \
	$(top_builddir)/librhosydd/librhosydd-@RSD_API_VERSION@.la \
	$(CLIENTS_LIBS) \
	$(CODE_COVERAGE_LDFLAGS) \
	$(AM_LIBADD) \
	$(NULL)

clients_rhosydd_client_LDFLAGS = \
	-no-undefined \
	$(WARN_LDFLAGS) \
	$(AM_LDFLAGS) \
	$(NULL)

# librhosydd library
lib_LTLIBRARIES += librhosydd/librhosydd-@RSD_API_VERSION@.la

rsdincludedir = $(includedir)/librhosydd-@RSD_API_VERSION@
rsd_headers = \
	librhosydd/attribute.h \
	librhosydd/proxy-vehicle.h \
	librhosydd/subscription.h \
	librhosydd/subscription-manager.h \
	librhosydd/types.h \
	librhosydd/utilities.h \
	librhosydd/vehicle.h \
	librhosydd/vehicle-manager.h \
	librhosydd/version.h \
	$(NULL)

# The following headers are private, and shouldn't be installed:
rsd_private_headers = \
	$(NULL)
nobase_rsdinclude_HEADERS = \
	$(rsd_headers) \
	$(NULL)

nobase_nodist_rsdinclude_HEADERS = \
	$(rsd_main_header) \
	$(NULL)
BUILT_SOURCES += $(nobase_nodist_rsdinclude_HEADERS)

rsd_sources = \
	librhosydd/attribute.c \
	librhosydd/proxy-vehicle.c \
	librhosydd/subscription.c \
	librhosydd/subscription-manager.c \
	librhosydd/utilities.c \
	librhosydd/vehicle.c \
	librhosydd/vehicle-manager.c \
	librhosydd/types.c \
	$(NULL)

rsd_main_header = librhosydd/rhosydd.h

$(rsd_main_header): Makefile
	$(AM_V_GEN){ \
		set -e; \
		echo "/* generated file */"; \
		echo "#ifndef RSD_H"; \
		echo "#define RSD_H"; \
		for h in $(rsd_headers); do \
			echo "#include <$$h>"; \
		done; \
		echo "#endif"; \
	} > $@.tmp && mv $@.tmp $@

rsd_public_headers = $(nobase_rsdinclude_HEADERS) $(nobase_nodist_rsdinclude_HEADERS)

dbus_rsd_built_sources = \
	librhosydd/vehicle-interface-generated.c \
	librhosydd/vehicle-interface-generated.h
BUILT_SOURCES += $(dbus_rsd_built_sources)

$(rsd_sources): $(dbus_rsd_built_sources)

EXTRA_DIST += \
	$(top_srcdir)/librhosydd/org.apertis.Rhosydd1.Vehicle.xml

$(dbus_rsd_built_sources): Makefile.am $(srcdir)/librhosydd/org.apertis.Rhosydd1.Vehicle.xml
	$(AM_V_GEN)gdbus-codegen \
		--interface-prefix org.apertis. \
		--c-namespace Rsdpriv \
		--interface-info-body \
		--output librhosydd/vehicle-interface-generated.c \
		$(srcdir)/librhosydd/org.apertis.Rhosydd1.Vehicle.xml \
		$(NULL)
	$(AM_V_GEN)gdbus-codegen \
		--interface-prefix org.apertis. \
		--c-namespace Rsdpriv \
		--interface-info-header \
		--output librhosydd/vehicle-interface-generated.h \
		$(srcdir)/librhosydd/org.apertis.Rhosydd1.Vehicle.xml \
		$(NULL)
	$(AM_V_GEN)gdbus-codegen \
		--interface-prefix org.apertis. \
		--c-namespace Rsdpriv \
		--generate-docbook librhosydd/docs/dbus \
		$(srcdir)/librhosydd/org.apertis.Rhosydd1.Vehicle.xml \
		$(NULL)

librhosydd_librhosydd_@RSD_API_VERSION@_la_SOURCES = \
	$(rsd_private_headers) \
	$(rsd_sources) \
	$(NULL)

librhosydd_librhosydd_@RSD_API_VERSION@_la_CPPFLAGS = \
	-I$(top_srcdir) \
	-I$(top_builddir) \
	-DG_LOG_DOMAIN=\"librhosydd\" \
	$(AM_CPPFLAGS) \
	$(NULL)

librhosydd_librhosydd_@RSD_API_VERSION@_la_CFLAGS = \
	$(RHOSYDD_CFLAGS) \
	$(CODE_COVERAGE_CFLAGS) \
	$(WARN_CFLAGS) \
	$(AM_CFLAGS) \
	$(NULL)

librhosydd_librhosydd_@RSD_API_VERSION@_la_LIBADD = \
	$(top_builddir)/libinternal/libinternal.la \
	$(RHOSYDD_LIBS) \
	$(CODE_COVERAGE_LDFLAGS) \
	$(AM_LIBADD) \
	$(NULL)

librhosydd_librhosydd_@RSD_API_VERSION@_la_LDFLAGS = \
	-version-info $(RSD_LT_VERSION) \
	-export-symbols-regex "^rsd_" \
	-no-undefined \
	$(WARN_LDFLAGS) \
	$(AM_LDFLAGS) \
	$(NULL)

# libcroesor library
lib_LTLIBRARIES += libcroesor/libcroesor-@CSR_API_VERSION@.la

csrincludedir = $(includedir)/libcroesor-@CSR_API_VERSION@
csr_headers = \
	libcroesor/backend.h \
	libcroesor/backend-manager.h \
	libcroesor/clock.h \
	libcroesor/peer-manager.h \
	libcroesor/service.h \
	libcroesor/static-vehicle.h \
	libcroesor/subscription-manager.h \
	libcroesor/vehicle-manager.h \
	libcroesor/vehicle-service.h \
	libcroesor/version.h \
	$(NULL)

# The following headers are private, and shouldn't be installed:
csr_private_headers = \
	$(NULL)
nobase_csrinclude_HEADERS = \
	$(csr_headers) \
	$(NULL)

nobase_nodist_csrinclude_HEADERS = \
	$(csr_main_header) \
	$(NULL)
BUILT_SOURCES += $(nobase_nodist_csrinclude_HEADERS)

csr_sources = \
	libcroesor/backend.c \
	libcroesor/backend-manager.c \
	libcroesor/peer-manager.c \
	libcroesor/service.c \
	libcroesor/subscription-manager.c \
	libcroesor/static-vehicle.c \
	libcroesor/vehicle-manager.c \
	libcroesor/vehicle-service.c \
	$(NULL)

csr_main_header = libcroesor/croesor.h

$(csr_main_header): Makefile
	$(AM_V_GEN){ \
		set -e; \
		echo "/* generated file */"; \
		echo "#ifndef CSR_H"; \
		echo "#define CSR_H"; \
		for h in $(csr_headers); do \
			echo "#include <$$h>"; \
		done; \
		echo "#endif"; \
	} > $@.tmp && mv $@.tmp $@

csr_public_headers = $(nobase_csrinclude_HEADERS) $(nobase_nodist_csrinclude_HEADERS)

dbus_csr_built_sources = \
	libcroesor/object-manager-interface-generated.c \
	libcroesor/object-manager-interface-generated.h
BUILT_SOURCES += $(dbus_csr_built_sources)

$(csr_sources): $(dbus_csr_built_sources)

EXTRA_DIST += \
	$(top_srcdir)/libcroesor/org.freedesktop.DBus.ObjectManager.xml

$(dbus_csr_built_sources): Makefile.am $(srcdir)/libcroesor/org.freedesktop.DBus.ObjectManager.xml
	$(AM_V_GEN)gdbus-codegen \
		--interface-prefix org.apertis. \
		--c-namespace Csrpriv \
		--interface-info-body \
		--output libcroesor/object-manager-interface-generated.c \
		$(srcdir)/libcroesor/org.freedesktop.DBus.ObjectManager.xml \
		$(NULL)
	$(AM_V_GEN)gdbus-codegen \
		--interface-prefix org.apertis. \
		--c-namespace Csrpriv \
		--interface-info-header \
		--output libcroesor/object-manager-interface-generated.h \
		$(srcdir)/libcroesor/org.freedesktop.DBus.ObjectManager.xml \
		$(NULL)
	$(AM_V_GEN)gdbus-codegen \
		--interface-prefix org.apertis. \
		--c-namespace Csrpriv \
		--generate-docbook libcroesor/docs/dbus \
		$(srcdir)/libcroesor/org.freedesktop.DBus.ObjectManager.xml \
		$(NULL)

libcroesor_libcroesor_@CSR_API_VERSION@_la_SOURCES = \
	$(csr_private_headers) \
	$(csr_sources) \
	$(NULL)

libcroesor_libcroesor_@CSR_API_VERSION@_la_CPPFLAGS = \
	-I$(top_srcdir) \
	-I$(top_builddir) \
	-DG_LOG_DOMAIN=\"libcroesor\" \
	-DLOCALEDIR=\""$(localedir)"\" \
	$(AM_CPPFLAGS) \
	$(NULL)

libcroesor_libcroesor_@CSR_API_VERSION@_la_CFLAGS = \
	$(CROESOR_CFLAGS) \
	$(CODE_COVERAGE_CFLAGS) \
	$(WARN_CFLAGS) \
	$(AM_CFLAGS) \
	$(NULL)

libcroesor_libcroesor_@CSR_API_VERSION@_la_LIBADD = \
	$(top_builddir)/libinternal/libinternal.la \
	$(top_builddir)/librhosydd/librhosydd-@RSD_API_VERSION@.la \
	$(CROESOR_LIBS) \
	$(CODE_COVERAGE_LDFLAGS) \
	$(AM_LIBADD) \
	$(NULL)

libcroesor_libcroesor_@CSR_API_VERSION@_la_LDFLAGS = \
	-version-info $(CSR_LT_VERSION) \
	-export-symbols-regex "^csr_" \
	-no-undefined \
	$(WARN_LDFLAGS) \
	$(AM_LDFLAGS) \
	$(NULL)

# Introspection
-include $(INTROSPECTION_MAKEFILE)
INTROSPECTION_GIRS =
INTROSPECTION_SCANNER_ARGS =
INTROSPECTION_COMPILER_ARGS = \
	--includedir=$(top_builddir)/librhosydd \
	--includedir=$(top_builddir)/libcroesor \
	$(NULL)
AM_DISTCHECK_CONFIGURE_FLAGS += --enable-introspection

if HAVE_INTROSPECTION
# librhosydd
librhosydd/Rhosydd-@RSD_API_VERSION@.gir: librhosydd/librhosydd-@RSD_API_VERSION@.la
librhosydd_Rhosydd_@RSD_API_VERSION@_gir_INCLUDES = GObject-2.0 Gio-2.0
librhosydd_Rhosydd_@RSD_API_VERSION@_gir_CFLAGS = \
	$(librhosydd_librhosydd_@RSD_API_VERSION@_la_CPPFLAGS) \
	$(librhosydd_librhosydd_@RSD_API_VERSION@_la_CFLAGS) \
	$(NULL)
librhosydd_Rhosydd_@RSD_API_VERSION@_gir_LIBS = librhosydd/librhosydd-@RSD_API_VERSION@.la
librhosydd_Rhosydd_@RSD_API_VERSION@_gir_FILES = $(rsd_sources) $(rsd_headers)
librhosydd_Rhosydd_@RSD_API_VERSION@_gir_NAMESPACE = Rhosydd
librhosydd_Rhosydd_@RSD_API_VERSION@_gir_EXPORT_PACKAGES = librhosydd-@RSD_API_VERSION@
librhosydd_Rhosydd_@RSD_API_VERSION@_gir_SCANNERFLAGS = \
	--nsversion=@RSD_API_VERSION@ \
	--identifier-prefix=Rsd \
	--symbol-prefix=rsd \
	--c-include="librhosydd/rhosydd.h" \
	$(WARN_SCANNERFLAGS) \
	$(NULL)

INTROSPECTION_GIRS += librhosydd/Rhosydd-@RSD_API_VERSION@.gir

# libcroesor
libcroesor/Croesor-@CSR_API_VERSION@.gir: libcroesor/libcroesor-@CSR_API_VERSION@.la librhosydd/Rhosydd-@RSD_API_VERSION@.gir
libcroesor_Croesor_@CSR_API_VERSION@_gir_INCLUDES = GObject-2.0 Gio-2.0 Polkit-1.0
libcroesor_Croesor_@CSR_API_VERSION@_gir_CFLAGS = \
	$(libcroesor_libcroesor_@CSR_API_VERSION@_la_CPPFLAGS) \
	$(libcroesor_libcroesor_@CSR_API_VERSION@_la_CFLAGS) \
	$(NULL)
libcroesor_Croesor_@CSR_API_VERSION@_gir_LIBS = libcroesor/libcroesor-@CSR_API_VERSION@.la
libcroesor_Croesor_@CSR_API_VERSION@_gir_FILES = $(csr_sources) $(csr_headers)
libcroesor_Croesor_@CSR_API_VERSION@_gir_NAMESPACE = Croesor
libcroesor_Croesor_@CSR_API_VERSION@_gir_EXPORT_PACKAGES = libcroesor-@CSR_API_VERSION@
libcroesor_Croesor_@CSR_API_VERSION@_gir_SCANNERFLAGS = \
	--nsversion=@CSR_API_VERSION@ \
	--identifier-prefix=Csr \
	--symbol-prefix=csr \
	--c-include="libcroesor/croesor.h" \
	--include-uninstalled=$(top_builddir)/librhosydd/Rhosydd-@RSD_API_VERSION@.gir \
	$(WARN_SCANNERFLAGS) \
	$(NULL)

INTROSPECTION_GIRS += libcroesor/Croesor-@CSR_API_VERSION@.gir

girdir = $(datadir)/gir-1.0
gir_DATA = $(INTROSPECTION_GIRS)

typelibdir = $(libdir)/girepository-1.0
typelib_DATA = $(INTROSPECTION_GIRS:.gir=.typelib)

CLEANFILES += $(gir_DATA) $(typelib_DATA)
endif

# Code coverage
include $(top_srcdir)/aminclude_static.am

CODE_COVERAGE_DIRECTORY = $(top_builddir)/librhosydd $(top_builddir)/libcroesor
CODE_COVERAGE_LCOV_OPTIONS = --base-directory $(abs_top_srcdir)

# Cleaning
EXTRA_DIST += \
	.arclint \
	.clang-format \
	autogen.sh \
	glib-tap.mk \
	rhosydd.doap \
	tap-test \
	$(NULL)

CLEANFILES += \
	`find "$(srcdir)" -type f -name .dirstamp -print` \
	$(NULL)

MAINTAINERCLEANFILES += \
	$(GITIGNORE_MAINTAINERCLEANFILES_TOPLEVEL) \
	$(GITIGNORE_MAINTAINERCLEANFILES_MAKEFILE_IN) \
	$(GITIGNORE_MAINTAINERCLEANFILES_M4_LIBTOOL) \
	INSTALL \
	ABOUT-NLS \
	build-aux/config.rpath \
	build-aux/tap-driver.sh \
	po/Makevars.template \
	po/Rules-quot \
	po/boldquot.sed \
	po/en@boldquot.header \
	po/en@quot.header \
	po/insert-header.sin \
	po/quot.sed \
	po/remove-potcdate.sin \
	po/rhosydd.pot \
	po/Makefile.in.in \
	$(NULL)

AM_DISTCHECK_CONFIGURE_FLAGS += --enable-documentation

# pkg-config data
# Note that the template file is called librhosydd.pc.in, but generates a
# versioned .pc file using some magic in AC_CONFIG_FILES.
pkgconfigdir = $(libdir)/pkgconfig
pkgconfig_DATA += librhosydd/librhosydd-$(RSD_API_VERSION).pc
DISTCLEANFILES += librhosydd/librhosydd-$(RSD_API_VERSION).pc
EXTRA_DIST += \
	librhosydd/librhosydd.pc.in \
	librhosydd/version.h.in \
	$(NULL)

# And for libcroesor.
pkgconfig_DATA += libcroesor/libcroesor-$(CSR_API_VERSION).pc
DISTCLEANFILES += libcroesor/libcroesor-$(CSR_API_VERSION).pc
EXTRA_DIST += \
	libcroesor/libcroesor.pc.in \
	libcroesor/version.h.in \
	$(NULL)

# Versioning

BUILT_SOURCES += $(top_srcdir)/.version
EXTRA_DIST += \
	build-aux/git-version-gen \
	$(top_srcdir)/.version \
	$(NULL)

dist-hook: dist-hook-git-version-gen
.PHONY: dist-hook-git.version-gen
dist-hook-git-version-gen:
	$(AM_V_GEN)echo $(VERSION) > $(distdir)/.tarball-version

$(top_srcdir)/.version:
	$(AM_V_GEN)echo $(VERSION) > $@-t && mv $@-t $@

# ChangeLog
@GENERATE_CHANGELOG_RULES@
dist-hook: dist-ChangeLog

-include $(top_srcdir)/git.mk
